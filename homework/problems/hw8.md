<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
</style>

Homework #8 Problems
====================

Ben Draut / CS 450 / Mar. 26, 2014

##

**1) Suppose that you are sampling a bandlimited signal whose highest frequency is 10 KHz. At what rate would you have to sample this signal assuming perfect reconstruction?** 

From the sampling theorem, we know that we can completely recover the signal if we sample at at least twice the highest frequency in the function. Therefore, if the highest frequency is 10 KHz, we need to sample at at least 20 KHz.

**At what rate would you have to sample it if the reconstruction filter was a low-pass filter with an imperfect cutoff at 10 KHz but allowed some frequencies up to 15 KHz?**

We would have to sample at $2 * 15 KHz = 30 KHz$.

##

**2) Given the following image locations and values, calculate the value of the image at $(2.7,4.8)$ using bilinear interpolation.**

| Location | Value    |
| -------- | -------- |
| (2,4)    | 7        |
| (2,5)    | 8        |
| (3,4)    | 8        |
| (3,5)    | 10       |

Using the formula for bilinear interpolation, we want to solve this equation:

$v(2.7, 4.8) = 2.7a + 4.8b + (2.7*4.8)c + d$

To do this, we need to solve the following system, generated from the 4 given points:

$7 = 2a + 4b + 8c + d$

$8 = 2a + 5b + 10c + d$

$8 = 3a + 4b + 12c + d$

$10 = 3a + 5b + 15c + d$

Solving this system yields:

$a = -3, b = -1, c = 1, d = 9$

Substituting these values back into the orignal equation gives:

$v(2.7, 4.8) = 2.7(-3) + 4.8(-1) + 2.7(4.8)(1) + 9 = 9.06$

**Matlab code:**

```matlab
syms a b c d;
[sol_a, sol_b, sol_c, sol_d] = ...
	solve(2*a + 4*b + 8*c + d == 7, ...
		  2*a + 5*b + 10*c + d == 8, ...
		  3*a + 4*b + 12*c + d == 8, ...
		  3*a + 5*b + 15*c + d == 10);

v = 2.7*sol_a + 4.8*sol_b + 2.7*4.8*sol_c + sol_d;
v = double(v);
disp(v);
```

##

**3) Use bilinear warping to produce a mapping from the quadrilateral $(0,0), (1,0), (1.1, 1.1), (0,1)$ to the unit  square.**

|    From   |   To  |
| --------- | ----- |
| (0,0)     | (0,0) |
| (1,0)     | (1,0) |
| (1.1,1.1) | (1,1) |
| (0,1)     | (0,1) |

The equations for bilinear warping are as follows:

$x' = r(x,y) = c_1x + c_2y + c_3xy + c_4$

$y' = s(x,y) = c_5x + c_6y + c_7xy + c_8$

We'll create two systems of four equations to solve for the coefficients.

For $x'$:

$0 = 0c_1 + 0c_2 + (0)(0)c_3 + c_4$

$1 = 1c_1 + 0c_2 + (0)(1)c_3 + c_4$

$1 = 1.1c_1 + 1.1c_2 + (1.1)(1.1)c_3 + c_4$

$0 = 0c_1 + 1c_2 + (0)(1)c_3 + c_4$

For $y'$:

$0 = 0c_5 + 0c_6 + (0)(0)c_7 + c_8$

$0 = 1c_5 + 0c_6 + (0)(1)c_7 + c_8$

$1 = 1.1c_5 + 1.1c_6 + (1.1)(1.1)c_7 + c_8$

$1 = 0c_5 + 1c_6 + (0)(1)c_7 + c_8$

Solving these systems (via MATLAB) yields the following values for coefficients:

| $c_n$ |   Value   |
| ----- | --------- |
|     1 |         1 |
|     2 |         0 |
|     3 | -0.082645 |
|     4 |         0 |
|     5 |         0 |
|     6 |         1 |
|     7 | -0.082645 |
|     8 |         0 |

So, our mapping functions become:

$x' = r(x,y) = (1)x + (0)y + (-0.082645)xy + 0$

$y' = s(x,y) = (0)x + (1)y + (-0.082645)xy + 0$

Which simplify to:

$x' = x - 0.082645xy$

$y' = y - 0.082645xy$

**Matlab code:**

```matlab
syms c_1 c_2 c_3 c_4 c_5 c_6 c_7 c_8;
[sol_c_1, sol_c_2, sol_c_3, sol_c_4] = ...
	solve(0*c_1 + 0*c_2 + (0*0)*c_3 + c_4 == 0, ...
		  1*c_1 + 0*c_2 + (0*1)*c_3 + c_4 == 1, ...
		  1.1*c_1 + 1.1*c_2 + (1.1*1.1)*c_3 + c_4 == 1, ...
		  0*c_1 + 1*c_2 + (0*1)*c_3 + c_4 == 0);

disp(strcat('c1: ', num2str(double(sol_c_1))));
disp(strcat('c2: ', num2str(double(sol_c_2))));
disp(strcat('c3: ', num2str(double(sol_c_3))));
disp(strcat('c4: ', num2str(double(sol_c_4))));

[sol_c_5, sol_c_6, sol_c_7, sol_c_8] = ...
	solve(0*c_5 + 0*c_6 + (0*0)*c_7 + c_8 == 0, ...
 		  1*c_5 + 0*c_6 + (0*1)*c_7 + c_8 == 0, ...
 		  1.1*c_5 + 1.1*c_6 + (1.1*1.1)*c_7 + c_8 == 1, ...
 		  0*c_5 + 1*c_6 + (0*1)*c_7 + c_8 == 1);

disp(strcat('c5: ', num2str(double(sol_c_5))));
disp(strcat('c6: ', num2str(double(sol_c_6))));
disp(strcat('c7: ', num2str(double(sol_c_7))));
disp(strcat('c8: ', num2str(double(sol_c_8))));
```	
