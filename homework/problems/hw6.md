Homework #6 Problems
====================

Ben Draut / CS 450 / Mar. 4, 2014

##

**Use the Convolution Theorem to obtain the Fourier Transform of a Triangular Function. (5 pts.)**

The convolution theorem states that $f(t) * g(t) \equiv F(u)G(u)$. 

We also know that a rectangular pulse function convolved with another rectangular pulse function yields a triangular function. Therefore, by the convolution theorem, we know that the Fourier transform of a Triangular function is equal to the Fourier transform of a rectangular pulse function squared. 

$\mathfrak{F}_{rect} = sinc(a\pi u)$

$\mathfrak{F}_{triangle} = sinc^2(a\pi u)$

Where $a$ represents the width of the rectangular pulse, and half the width of the triangular function.

##

**Let $g(x)$ be:**

**$1$ if $|n| < 1/2$**

**$0$ otherwise**

**Let $f(x) = \cos(2\pi x)$**

**Let $h(x) = g(x)f(x)$. Find $H(u)$.**

If $h(x) = g(x)f(x)$, then we know by the convolution theorem that $H(u) = \mathfrak{F}(f) * \mathfrak{F}(g)$

$\mathfrak{F}(f) = 1/2[\delta(u+1) + \delta(u-1)]$

$\mathfrak{F}(g) = sinc(a\pi u)$

We know that a delta function convolved with another signal is that signal itself. Therefore:

$\mathfrak{F}(f) * \mathfrak{F}(g) = H(u) = 1/2[sinc(\pi u + 1) + sinc(\pi u -1)]$

<!-- $\mathfrak{F}(h) = H(u) = \int_{-\infty}^{\infty} \mathrm{1/2[\delta(u+1) + \delta(u-1)]sinc(a\pi (\tau-u))}$ -->

**Also, carefully draw/plot $H(u)$ and label the axes.**

```matlab
u = linspace(-pi, pi, 200);
y = 0.5*(sinc(pi*(u+1)) + sinc(pi*(u-1)));
figure;
fh = gcf;
hold on;
plot(u,y);
grid on;
title('H(u)');
xlabel('Frequency (u)');
ylabel('Amplitude');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_hw_1.png', '-dpng');
```

![](../img/hw6/figure_hw_1.png)
