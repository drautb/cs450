Homework #1 Writeup
===================

Ben Draut / CS 450 / Jan. 10, 2014

For this class, I've elected to just use the provided toolkit and write my programs in Java.

**Grayscale Histogram:**

![](img/hw1-hist.png)
