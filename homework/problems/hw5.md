<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
</style>

Homework #5 Problems
====================

Ben Draut / CS 450 / Feb. 18, 2014

##

**1) Show that the Fourier Transform is linear. (You'll need to use the Fourier Transform integral to show this.)**

Fourier Transform:

$F(u) = \int_{-\infty}^{\infty} \mathrm{f(x)e^{2\pi i u x}} \mathrm{dx}$

Let $f_1(t)$ and $f_2(t)$ be functions, and $c_1$ and $c_2$ be constants.

If $F(u)$ is linear, then $F(c_1f_1(t) + c_2f_2(t)) = c_1F(f_1(t)) + c_2F(f_2(t))$

Proof:

$F(c_1f_1(t) + c_2f_2(t)) = \int_{-\infty}^{\infty} \mathrm{(c_1f_1(t) + c_2f_2(t)) e^{2\pi i u t}} \mathrm{dt}$

$F(c_1f_1(t) + c_2f_2(t)) = \int_{-\infty}^{\infty} \mathrm{c_1f_1(t) e^{2\pi i u t}} \mathrm{dt} + \int_{-\infty}^{\infty} \mathrm{c_2f_2(t) e^{2\pi i u t}} \mathrm{dt}$

$F(c_1f_1(t) + c_2f_2(t)) = c_1 \int_{-\infty}^{\infty} \mathrm{f_1(t) e^{2\pi i u t}} \mathrm{dt} + c_2 \int_{-\infty}^{\infty} \mathrm{f_2(t) e^{2\pi i u t}} \mathrm{dt}$

$F(c_1f_1(t) + c_2f_2(t)) = c_1 F(f_1(t)) + c_2 F(f_2(t))$

##

**2) Compute by hand the DFT of the signal f = [ 1, 1, 1, 1]. Make sure to show your work.**

DFT:

$F(u) = \frac{1}{M} \sum_{x=0}^{M-1}{f(x) \cos(2\pi ux/M)} - i \frac{1}{M} \sum_{x=0}^{M-1} {f(x) \sin(2\pi ux/M)}$

|    $u$    | $x$ | $f(x)$ |      $f(x)\cos(2\pi ux/M)$      |      $f(x)\sin(2\pi ux/M)$      |
| --------- | --- | ------ | ------------------------------- | ------------------------------- |
| 0         |   0 |      1 | $f(x)\cos(2\pi\*0\*0/4) = 1.0$  | $f(x)\sin(2\pi\*0\*0/4) = 0.0$  |
|           |   1 |      1 | $f(x)\cos(2\pi\*0\*1/4) = 1.0$  | $f(x)\sin(2\pi\*0\*1/4) = 0.0$  |
|           |   2 |      1 | $f(x)\cos(2\pi\*0\*2/4) = 1.0$  | $f(x)\sin(2\pi\*0\*2/4) = 0.0$  |
|           |   3 |      1 | $f(x)\cos(2\pi\*0\*3/4) = 1.0$  | $f(x)\sin(2\pi\*0\*3/4) = 0.0$  |
| Sum (u=0) |     |        | 4.0                             | 0.0                             |
| 1         |   0 |      1 | $f(x)\cos(2\pi\*1\*0/4) = 1.0$  | $f(x)\sin(2\pi\*1\*0/4) = 0.0$  |
|           |   1 |      1 | $f(x)\cos(2\pi\*1\*1/4) = 0.0$  | $f(x)\sin(2\pi\*1\*1/4) = 1.0$  |
|           |   2 |      1 | $f(x)\cos(2\pi\*1\*2/4) = -1.0$ | $f(x)\sin(2\pi\*1\*2/4) = 0.0$  |
|           |   3 |      1 | $f(x)\cos(2\pi\*1\*3/4) = 0.0$  | $f(x)\sin(2\pi\*1\*3/4) = -1.0$ |
| Sum (u=1) |     |        | 0.0                             | 0.0                             |
| 2         |   0 |      1 | $f(x)\cos(2\pi\*2\*0/4) = 1.0$  | $f(x)\sin(2\pi\*2\*0/4) = 0.0$  |
|           |   1 |      1 | $f(x)\cos(2\pi\*2\*1/4) = -1.0$ | $f(x)\sin(2\pi\*2\*1/4) = 0.0$  |
|           |   2 |      1 | $f(x)\cos(2\pi\*2\*2/4) = 1.0$  | $f(x)\sin(2\pi\*2\*2/4) = 0.0$  |
|           |   3 |      1 | $f(x)\cos(2\pi\*2\*3/4) = -1.0$ | $f(x)\sin(2\pi\*2\*3/4) = 0.0$  |
| Sum (u=2) |     |        | 0.0                             | 0.0                             |
| 3         |   0 |      1 | $f(x)\cos(2\pi\*3\*0/4) = 1.0$  | $f(x)\sin(2\pi\*3\*0/4) = 0.0$  |
|           |   1 |      1 | $f(x)\cos(2\pi\*3\*1/4) = 0.0$  | $f(x)\sin(2\pi\*3\*1/4) = -1.0$ |
|           |   2 |      1 | $f(x)\cos(2\pi\*3\*2/4) = -1.0$ | $f(x)\sin(2\pi\*3\*2/4) = 0.0$  |
|           |   3 |      1 | $f(x)\cos(2\pi\*3\*3/4) = 0.0$  | $f(x)\sin(2\pi\*3\*3/4) = 1.0$  |
| Sum (u=3) |     |        | 0.0                             | 0.0                             |


Results:

| $u$ |  $F(u)$  | Mag. F(u) | $\phi F(u)$ |
| --- | -------- | --------- | ----------- |
|   0 | $1 - 0i$ |         1 | 0           |
|   1 | $0 - 0i$ |         0 | undefined   |
|   2 | $0 - 0i$ |         0 | undefined   |
|   3 | $0 - 0i$ |         0 | undefined   |

This result makes sense, because our original signal was just a flat line.


