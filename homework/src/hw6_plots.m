N = 256;
s = 5;

% Simple Sine and Cosines
figure;
fh = gcf;
image = sin(2*pi*s*[1:N]'/N) * ones([1 N]);
image = image';
imshow(image);
title('f(x,y) with s = 5');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_1.png', '-dpng');
close(fh);

figure;
fh = gcf;
trans = fft2(image);
trans = fftshift(trans);
trans = abs(trans);
trans = log(trans + 1);
trans = mat2gray(trans);
imshow(trans, []);
title('|Transform|');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_2.png', '-dpng');
close(fh);

figure;
fh = gcf;
s = 15;
image = sin(2*pi*s*[1:N]'/N) * ones([1 N]);
image = image';
imshow(image);
title('f(x,y) with s = 15');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_3.png', '-dpng');
close(fh);

figure;
fh = gcf;
trans = fft2(image);
trans = fftshift(trans);
trans = abs(trans);
trans = log(trans + 1);
trans = mat2gray(trans);
imshow(trans, []);
title('|Transform|');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_4.png', '-dpng');
close(fh);

figure;
fh = gcf;
s = 25;
image = sin(2*pi*s*[1:N]'/N) * ones([1 N]);
image = image';
imshow(image);
title('f(x,y) with s = 25');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_5.png', '-dpng');
close(fh);

figure;
fh = gcf;
trans = fft2(image);
trans = fftshift(trans);
trans = abs(trans);
trans = log(trans + 1);
trans = mat2gray(trans);
imshow(trans, []);
title('|Transform|');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_6.png', '-dpng');
close(fh);

% Switch Directions
s = 10;
figure;
fh = gcf;
image = sin(2*pi*s*[1:N]'/N) * ones([1 N]);
imshow(image);
title('f(x,y) with s = 10');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_7.png', '-dpng');
close(fh);

figure;
fh = gcf;
trans = fft2(image);
trans = fftshift(trans);
trans = abs(trans);
trans = log(trans + 1);
trans = mat2gray(trans);
imshow(trans, []);
title('|Transform|');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_8.png', '-dpng');
close(fh);

figure;
fh = gcf;
s = 20;
image = sin(2*pi*s*[1:N]'/N) * ones([1 N]);
imshow(image);
title('f(x,y) with s = 20');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_9.png', '-dpng');
close(fh);

figure;
fh = gcf;
trans = fft2(image);
trans = fftshift(trans);
trans = abs(trans);
trans = log(trans + 1);
trans = mat2gray(trans);
imshow(trans, []);
title('|Transform|');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_10.png', '-dpng');
close(fh);

figure;
fh = gcf;
s = 30;
image = sin(2*pi*s*[1:N]'/N) * ones([1 N]);
imshow(image);
title('f(x,y) with s = 30');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_11.png', '-dpng');
close(fh);

figure;
fh = gcf;
trans = fft2(image);
trans = fftshift(trans);
trans = abs(trans);
trans = log(trans + 1);
trans = mat2gray(trans);
imshow(trans, []);
title('|Transform|');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_12.png', '-dpng');
close(fh);

%% ADDITION
figure;
fh = gcf;
s = 5;
image = sin(2*pi*s*[1:N]'/N) * ones([1 N]);
image = image';
s = 10;
image2 = sin(2*pi*s*[1:N]'/N) * ones([1 N]);
image = image + image2;
imshow(image);
title('Addition');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_13.png', '-dpng');
close(fh);

figure;
fh = gcf;
trans = fft2(image);
trans = fftshift(trans);
trans = abs(trans);
trans = log(trans + 1);
trans = mat2gray(trans);
imshow(trans, []);
title('|Transform|');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_14.png', '-dpng');
close(fh);

%% ROTATION
figure;
fh = gcf;
rotImage = imrotate(image, 30);
imshow(rotImage);
title('Rotation');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_15.png', '-dpng');
close(fh);

figure;
fh = gcf;
trans = fft2(rotImage);
trans = fftshift(trans);
trans = abs(trans);
trans = log(trans + 1);
trans = mat2gray(trans);
imshow(trans, []);
title('|Transform|');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_16.png', '-dpng');
close(fh);

%% MULTIPLICATION
figure;
fh = gcf;
s = 5;
image = sin(2*pi*s*[1:N]'/N) * ones([1 N]);
image = image';
s = 10;
image2 = sin(2*pi*s*[1:N]'/N) * ones([1 N]);
image = image .* image2;
imshow(image);
title('Addition');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_17.png', '-dpng');
close(fh);

figure;
fh = gcf;
trans = fft2(image);
trans = fftshift(trans);
trans = abs(trans);
trans = log(trans + 1);
trans = mat2gray(trans);
imshow(trans, []);
title('|Transform|');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_18.png', '-dpng');
close(fh);

%% PART E - BALL AND GULL
ball = imread('../img/hw6/ball.pgm');

figure;
fh = gcf;
ballTrans = fft2(ball);
ballTrans = fftshift(ballTrans);
ballTrans = abs(ballTrans);
ballTrans = log(ballTrans + 1);
ballTrans = mat2gray(ballTrans);
imshow(ballTrans, []);
title('|Transform|');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_19.png', '-dpng');
close(fh);

gull = imread('../img/hw6/gull.pgm');

figure;
fh = gcf;
gullTrans = fft2(gull);
gullTrans = fftshift(gullTrans);
gullTrans = abs(gullTrans);
gullTrans = log(gullTrans + 1);
gullTrans = mat2gray(gullTrans);
imshow(gullTrans, []);
title('|Transform|');
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/figure_20.png', '-dpng');
close(fh);

%% Switch Phase/Mag...

ballTrans = fft2(ball);
ballReal = real(ballTrans);
ballImag = imag(ballTrans);
ballMag = sqrt(ballReal.^2 + ballImag.^2);
ballPhase = atan2(ballImag, ballReal);

gullTrans = fft2(gull);
gullReal = real(gullTrans);
gullImag = imag(gullTrans);
gullMag = sqrt(gullReal.^2 + gullImag.^2);
gullPhase = atan2(gullImag, gullReal);

imagBG = ballMag .* sin(gullPhase);
realBG = ballMag .* cos(gullPhase);
newBallImage = ifft2(realBG + imagBG);
figure;
fh = gcf;
imshow(uint8(real(newBallImage)));
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/ball-mag.png', '-dpng');
close(fh);

imagGB = gullMag .* sin(ballPhase);
realGB = gullMag .* cos(ballPhase);
newGullImage = ifft2(realGB + imagGB);
figure;
fh = gcf;
imshow(uint8(real(newGullImage)));
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw6/gull-mag.png', '-dpng');
close(fh);


