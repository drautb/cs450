<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
.clear {
    clear:both;
}
</style>

Homework #2 Problems
====================

Ben Draut / CS 450 / Jan. 13, 2014

**1. Suppose that a 32x32 image with 8 grey levels has the following histogram:**

|       |    |     |     |     |    |    |     |    |
| ----- | -- | --- | --- | --- | -- | -- | --- | -- |
| Value | 0  | 1   | 2   | 3   | 4  | 5  | 6   | 7  |
| Count | 62 | 100 | 200 | 100 | 50 | 50 | 400 | 62 |

**Calculate the grey-level transformation that performs histogram equalization on this image and sketch what the transformation function looks like.**

Transformation:

<img src="img/hw2-transform-function.png" style="float:right; width:68%"/>

| $s_k$ | $(L - 1) * \Sigma{p_r(r_k)}$ |
| ----- | ---------------------------- |
| 0     | 0.4238                       |
| 1     | 1.1074                       |
| 2     | 2.4746                       |
| 3     | 3.1582                       |
| 4     | 3.5000                       |
| 5     | 3.8418                       |
| 6     | 6.5762                       |
| 7     | 7.0000                       |

<div class="clear"></div>

**2. Suppose that you wanted to shape the histogram of the above image to match that of a second image:**

|       |   |     |   |   |   |     |   |   |
| ----- | - | --- | - | - | - | --- | - | - |
| Value | 0 | 1   | 2 | 3 | 4 | 5   | 6 | 7 |
| Count | 0 | 512 | 0 | 0 | 0 | 512 | 0 | 0 |

**Use the histogram specification (matching) technique described in class and your textbook to devise a grey-level transformation that shapes the histogram of the first image to look like the second. Make sure to show all intermediate steps in the calculations. Apply your transformation to the first image and compare the resulting histogram to that of the second image.**

<!-- Be careful: the continuous math has some discontinuities when you do it with discrete values (especially function inverses), so you'll have to adopt a strategy to handle certain cases. If two input values map to the same output value, what does that imply about the image? How can you use that to handle inverting the mapping? Similarly, what happens if no input values map onto a particular output value? What does that imply, and how can you use that? Make sure you describe these cases and how you handle them. -->

**Step 1:**

We have the equalization transformation from problem 1:

| $k$   | $(L - 1) * \Sigma{p_r(r_k)}$ | $s_k$   |
| ----- | ---------------------------- | ------- |
| 0     | 0.4238                       | 0       | 
| 1     | 1.1074                       | 1       |
| 2     | 2.4746                       | 2       |
| 3     | 3.1582                       | 3       |
| 4     | 3.5000                       | 4       |
| 5     | 3.8418                       | 4       |
| 6     | 6.5762                       | 7       |
| 7     | 7.0000                       | 7       |

**Step 2:**

Compute all values of the transformation function _G_, based on the specified histogram.

| $z_q$ | $p_z(z_q)$ | $(L-1) * \Sigma{p_z(z_q)}$ | $G(z_q)$ |
| ----- | ---------- | -------------------------- | -------- |
| 0     | 0.0000     | 0                          | 0        |
| 1     | 0.5000     | 3.5000                     | 4        | 
| 2     | 0.0000     | 3.5000                     | 4        |
| 3     | 0.0000     | 3.5000                     | 4        |
| 4     | 0.0000     | 3.5000                     | 4        | 
| 5     | 0.5000     | 7.0000                     | 7        |
| 6     | 0.0000     | 7.0000                     | 7        |
| 7     | 0.0000     | 7.0000                     | 7        |

**Step 3:**

Find mapping from _s_ to _z_. If $G(z_q)$ was not unique, I chose the lowest $z_q$ according to the textbook's recommendation.

| $s_k$ | $G(z_q)$ | $z_q$ |
| ----- | -------- | ----- |
| 0     | 0        | 0     |
| 1     | 0        | 0     |
| 2     | 0        | 0     |
| 3     | 4        | 1     |
| 4     | 4        | 1     |
| 4     | 4        | 1     |
| 7     | 7        | 5     |
| 7     | 7        | 5     |

**Step 4:**

Form the specified image.

<div>
    <img src="img/hw2-original-histogram.png" style="width:32%"/>
    <img src="img/hw2-equalized-histogram.png" style="width:33%"/>
    <img src="img/hw2-specified-histogram.png" style="width:33%"/>
    <div class="clear"></div>
</div>


**3. Suppose that you have a scanned document with text (near black), background (near white), and several grey-level images. After examining the document, you find that the image mostly have values in the range [50,200]. Devise a grey-level windowing approach that will best enhance the contrast of the document. Would histogram equalization be expected to do better?**

A grey-level windowing approach would want to perform histogram stretching just over the middle portion of the histogram. This would improve the contrast of the images in the document, without compromising the text. The text could be further improved by using some kind of thresholds. For example, all values less than 75 become zero, while all values greater than 175 become 255. 

Histogram equalization wouldn't do better than this. Equalizing the histogram would cause the contrast between the text and the blank page to increase, but the images would remain washed out.

**Textbook 3.7: Suppose that a digital image is subjected to histogram equalization. Show that a second pass of histogram equalization (on the histogram-equalized image) will produce exactly the same result as the first pass.**

Histogram equalization is defined by: $s_k = T(r_k) = \sum_{j=0}^{k} p_r(r_j)$

$p_r(r_j)$ can also be expressed as $\frac{n_{rj}}{n}$, where $n_{rj}$ is the number of pixels in with intensity _j_, and _n_ is the total number of pixels.

So, $s_k$ can be written $s_k = T(r_k) = \sum_{j=0}^{k} \frac{n_{rj}}{n}$

We know that every pixel with value $r_k$ gets mapped to value $s_k$, so we also know that $n_{rk} = n_{sk}$.

The second pass of equalization would look like this:

$v_k = T(s_k) = \sum_{j=0}^{k} \frac{n_{sj}}{n}$

But because we know that $n_{rk} = n_{sk}$, we can deduce that

$v_k = T(s_k) = \sum_{j=0}^{k} \frac{n_{sj}}{n} = \sum_{j=0}^{k} \frac{n_{rj}}{n} = s_k$,

which shows that the second pass did not change the histogram.

**Textbook 3.11: An image with intensities in the range [0,1] has the PDF $p_r(r)$ shown in the following diagram. It is desired to transform the intensity levels of this image so that they will have the specified $p_z(z)$ shown. Assume continuous quantities and find the transformation (in terms of $r$ and $z$) that will accomplish this.**

$p_r(r) = -2r + 2$, and $p_z(z) = 2z$.

First, we calculate the equalization function:

$T(r) = \int_0^r (-2r + 2)dw = -r^2 + 2r$

Then the specifcation:

$G(z) = \int_0^z (2z)dz = z^2$

$z = G^-1(v) = \sqrt{v}$, so $z = \sqrt{-r^2 + 2r}$.

