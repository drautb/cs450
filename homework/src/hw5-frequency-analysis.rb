#!/usr/bin/env ruby

# Ruby program to get the n most significant frequencies in 
# a signal.
#
# Usage: <command> "signal data file" n-frequencies

require "fftw3"
require "narray"

options = {
  :sig_file => ARGV[0],
  :n_freqs => ARGV[1]
}

signal_data = []
File.open(options[:sig_file], "r") do |infile|
    while (line = infile.gets)
        signal_data.push(line.to_f)
    end
end

signal_data = NArray.to_na(signal_data)

transform = FFTW3.fft(signal_data, -1) / (signal_data.length / 2.0)

magnitude = []
transform.each do |n|
  magnitude.push(Math.sqrt(n.real**2 + n.imag**2))
end

freqs = []
for index in 0 ... magnitude.size / 2
  freqs.push({:frequency => index, :magnitude => magnitude[index]})
end

freqs = freqs.sort_by { |hsh| hsh[:magnitude] }.reverse

for index in 0 ... options[:n_freqs].to_i
  puts "Frequency: #{freqs[index][:frequency]}\t\tMagnitude: #{freqs[index][:magnitude]}"
end


