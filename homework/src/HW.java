import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.File;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collections;
import javax.imageio.*;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.*;
import javax.swing.border.TitledBorder;


public class HW
{
	private static final int HISTOGRAM_LENGTH = 256;

	public static void main(String[] args)
	{
		HW hw = new HW();
		CS450.run(hw);
		
		for (String name : ImageIO.getReaderFormatNames())
		{
			System.out.println(name);
		}
	}
	
	public void doOpen()
	{
		BufferedImage img = CS450.openImage();
		
		if (img != null)
		{
			CS450.setImageA(img);
		}
	}

	// public void doThreshold()
	// {
	// 	String threshold = CS450.prompt("threshold (0 - 255)", "128");
	// 	if (threshold == null) return;
	// 	int t = Integer.parseInt(threshold);
		
	// 	BufferedImage inputImage = CS450.getImageA();
	// 	int width = inputImage.getWidth();
	// 	int height = inputImage.getHeight();
	// 	BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		
	// 	WritableRaster in = inputImage.getRaster();
	// 	WritableRaster out = outputImage.getRaster();
		
	// 	for (int y = 0; y < height; y++)
	// 	{
	// 		for (int x = 0; x < width; x++)
	// 		{
	// 			int val = in.getSample(x, y, 0);
				
	// 			if (val < t)
	// 			{
	// 				out.setSample(x, y, 0, 0); // black
	// 			}
	// 			else
	// 			{
	// 				out.setSample(x, y, 0, 255); // white
	// 			}
	// 		}
	// 	}
		
	// 	CS450.setImageB(outputImage);
	// }
/*
	public void doColor_Filter()
	{
		String[] choices = {"RED", "GREEN", "BLUE"};
		String colorChannel = CS450.prompt("color", choices, "GREEN");
		if (colorChannel == null) return;
		
		BufferedImage inputImage = CS450.getImageA();
		int width = inputImage.getWidth();
		int height = inputImage.getHeight();
		BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		WritableRaster in = inputImage.getRaster();
		WritableRaster out = outputImage.getRaster();
		
		int channel = 0; // defaults to RED filter
		
		if (colorChannel.equals("GREEN"))
		{
			channel = 1;
		}
		else if (colorChannel.equals("BLUE"))
		{
			channel = 2;
		}
		
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				int val = in.getSample(x, y, channel);
				
				out.setSample(x, y, channel, val);
			}
		}
		
		CS450.setImageB(outputImage);
	}

	/**
	 * Converts the image in memory to grayscale according to the following
	 * formula: Grey = 0.299 Red + 0.587 Green + 0.114 Blue
	 */
/*
	public void doGrayscale()
	{
		BufferedImage inputImage = CS450.getImageA();
		int width = inputImage.getWidth();
		int height = inputImage.getHeight();
		BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		WritableRaster in = inputImage.getRaster();
		WritableRaster out = outputImage.getRaster();

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				int val = (int)(0.299 * in.getSample(x, y, 0) +
								0.587 * in.getSample(x, y, 1) + 
								0.114 * in.getSample(x, y, 2));

				out.setSample(x, y, 0, val);
				out.setSample(x, y, 1, val);
				out.setSample(x, y, 2, val);
			}
		}

		CS450.setImageB(outputImage);
	}
*/	
	/**
	 * This function produces a vector of data describing the image histogram
	 * in MATLAB format. Once the data is in MATLAB, use 'bar(data)' to display
	 * the histogram.
	 */
	public void doHistogram()
	{
		BufferedImage inputImage = CS450.getImageA();
		int[] histogram = getHistogramData(inputImage);

		dumpArray(histogram, "histogram");
	}

	/**
	 * This function performs histogram equalization on the image.
	 */
/*
	public void doEqualize() 
	{
		BufferedImage inputImage = CS450.getImageA();
		int width = inputImage.getWidth();
		int height = inputImage.getHeight();
		int pixelCount = width * height;

		int[] histogram = getHistogramData(inputImage);
		int L = histogram.length - 1;
		double[] probDist = new double[histogram.length];
		int[] sValues = new int[histogram.length];

		// Initialize probability distribution
		for (int intensityLevel=0; intensityLevel<histogram.length; intensityLevel++) 
		{
			probDist[intensityLevel] = histogram[intensityLevel] / (double)pixelCount;
		}

		// Calculate the raw s-values
		double probabilitySum = 0.0;
		for (int sValueIdx=0; sValueIdx<sValues.length; sValueIdx++) 
		{
			probabilitySum = 0.0;
			for (int pIdx=0; pIdx<=sValueIdx; pIdx++) 
			{
				probabilitySum += probDist[pIdx];
			}
			// Round the raw s-values to the nearest whole number
			sValues[sValueIdx] = (int)(L * probabilitySum + 0.5);
		}

		// dumpArray(sValues, "sValues");

		// At this point, we have the s-values rounded to their nearest integer.
		// We need to now map each pixel from it's current intensity, to its
		// new equalized intensity.
		BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		WritableRaster in = inputImage.getRaster();
		WritableRaster out = outputImage.getRaster();

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				int val = sValues[in.getSample(x, y, 0)];

				out.setSample(x, y, 0, val);
				out.setSample(x, y, 1, val);
				out.setSample(x, y, 2, val);
			}
		}

		CS450.setImageB(outputImage);
	}

	public void doMean_Filter()
	{
		String[] choices = {"3x3", "5x5", "7x7", "9x9"};
		String gridSizeStr = CS450.prompt("Filter Size:", choices, "3x3");
		if (gridSizeStr == null) return;

		// GridSize is the number of cells each direction to check:
		// 3x3 => 1
		// 5x5 => 2
		// 7x7 => 3
		// 9x9 => 4
		int gridSize = (Integer.parseInt(gridSizeStr.substring(0,1)) - 1) / 2;

		BufferedImage inputImage = CS450.getImageA();
		int width = inputImage.getWidth();
		int height = inputImage.getHeight();
		BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		WritableRaster in = inputImage.getRaster();
		WritableRaster out = outputImage.getRaster();

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				int val = (int)getMean(in, x, y, width, height, gridSize);
				
				out.setSample(x, y, 0, val);
				out.setSample(x, y, 1, val);
				out.setSample(x, y, 2, val);
			}
		}
		
		CS450.setImageB(outputImage);
	}

	public void doMedian_Filter() {
		String[] choices = {"3x3", "5x5", "7x7", "9x9"};
		String gridSizeStr = CS450.prompt("Filter Size:", choices, "3x3");
		if (gridSizeStr == null) return;

		// GridSize is the number of cells each direction to check:
		// 3x3 => 1
		// 5x5 => 2
		// 7x7 => 3
		// 9x9 => 4
		int gridSize = (Integer.parseInt(gridSizeStr.substring(0,1)) - 1) / 2;

		BufferedImage inputImage = CS450.getImageA();
		int width = inputImage.getWidth();
		int height = inputImage.getHeight();
		BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		WritableRaster in = inputImage.getRaster();
		WritableRaster out = outputImage.getRaster();

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				int val = (int)getMedian(in, x, y, width, height, gridSize);
				
				out.setSample(x, y, 0, val);
				out.setSample(x, y, 1, val);
				out.setSample(x, y, 2, val);
			}
		}
		
		CS450.setImageB(outputImage);
	}

	public void doSobel_Kernel() {
		String[] choices = {"X", "Y", "Both", "Gradient"};
		String dimension = CS450.prompt("Dimension:", choices, "X");
		if (dimension == null) return;

		BufferedImage inputImage = CS450.getImageA();
		int width = inputImage.getWidth();
		int height = inputImage.getHeight();
		BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		WritableRaster in = inputImage.getRaster();
		WritableRaster out = outputImage.getRaster();

		if (dimension == "X") {
			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					int val = (int)(getSobelX(in, x, y) / 8.0);
					
					out.setSample(x, y, 0, val);
					out.setSample(x, y, 1, val);
					out.setSample(x, y, 2, val);
				}
			}
		} else if (dimension == "Y") {
			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					int val = (int)(getSobelY(in, x, y) / 8.0);
					
					out.setSample(x, y, 0, val);
					out.setSample(x, y, 1, val);
					out.setSample(x, y, 2, val);
				}
			}
		} else if (dimension == "Both") {
			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					int val = (int)((getSobelX(in, x, y) + 
									 getSobelY(in, x, y))
									/ 8.0);
					
					out.setSample(x, y, 0, val);
					out.setSample(x, y, 1, val);
					out.setSample(x, y, 2, val);
				}
			}
		} else if (dimension == "Gradient") {
			int min = 255, max = 0;
			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					int val = (int)Math.sqrt(Math.pow(getSobelX(in, x, y), 2) + 
								     		 Math.pow(getSobelY(in, x, y), 2));
					if (val < min)
						min = val;
					else if (val > max)
						max = val;			
				}
			}

			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					int val = (int)Math.sqrt(Math.pow(getSobelX(in, x, y), 2) + 
								     		 Math.pow(getSobelY(in, x, y), 2));
					val = 255 * (val - min) / (max - min);
					
					out.setSample(x, y, 0, val);
					out.setSample(x, y, 1, val);
					out.setSample(x, y, 2, val);
				}
			}
		} 
		
		CS450.setImageB(outputImage);
	}

	public void doLaplacian_Kernel() {
		BufferedImage inputImage = CS450.getImageA();
		int width = inputImage.getWidth();
		int height = inputImage.getHeight();
		BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		WritableRaster in = inputImage.getRaster();
		WritableRaster out = outputImage.getRaster();

		int min = 255, max = 0;

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				int val = (int)getLaplacian(in, x, y);
				if (val < min)
					min = val;
				else if (val > max)
					max = val;
			}
		}

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				int val = (int)getLaplacian(in, x, y);
				val = 255 * (val - min) / (max - min);

				out.setSample(x, y, 0, val);
				out.setSample(x, y, 1, val);
				out.setSample(x, y, 2, val);
			}
		}
		
		CS450.setImageB(outputImage);
	}
*/

	public void doMagnify() {
		String fStr = CS450.prompt("Magnification Factor:");
		int f = Integer.parseInt(fStr);

		BufferedImage inputImage = CS450.getImageA();
		int width = inputImage.getWidth();
		int height = inputImage.getHeight();
		int newWidth = width * f;
		int newHeight = height * f;

		BufferedImage outputImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
		WritableRaster in = inputImage.getRaster();
		WritableRaster out = outputImage.getRaster();

		for (int y = 0; y < newHeight; y++)
		{
			for (int x = 0; x < newWidth; x++)
			{
				double oldX = (double)x / (double)f;
				double oldY = (double)y / (double)f;

				int val = 0;
				if (oldX == Math.floor(oldX) && oldY == Math.floor(oldY)) {
					val = in.getSample((int)oldX, (int)oldY, 0);	
				} else { // Interpolate value
					double localX = oldX - Math.floor(oldX);
					double localY = oldY - Math.floor(oldY);

					int xFloor = (int)Math.floor(oldX);
					int xCeil = (int)Math.ceil(oldX);
					int yFloor = (int)Math.floor(oldY);
					int yCeil = (int)Math.ceil(oldY);

					xFloor = xFloor < 0 ? 0 : xFloor;
					yFloor = yFloor < 0 ? 0 : yFloor;
					xCeil = xCeil > width - 1 ? width - 1 : xCeil;
					yCeil = yCeil > height - 1 ? height - 1 : yCeil;

					double p1 = (double)in.getSample(xFloor, yFloor, 0);
					double p2 = (double)in.getSample(xCeil, yFloor, 0);
					double p3 = (double)in.getSample(xCeil, yCeil, 0);
					double p4 = (double)in.getSample(xFloor, yCeil, 0);

					val = (int)(p1*(1-localX)*(1-localY) + 
								p2*(1-localY)*localX + 
								p3*localX*localY + 
								p4*(1-localX)*localY);
				}
				
				out.setSample(x, y, 0, val);
				out.setSample(x, y, 1, val);
				out.setSample(x, y, 2, val);
			}
		}
		
		CS450.setImageB(outputImage);
	}

	public void doReduce() {
		String fStr = CS450.prompt("Reduction Factor:");
		int factor = Integer.parseInt(fStr);

		CS450.setImageB(CS450.getImageA());

		int reductions = (int)(Math.log(factor) / Math.log(2));
		System.out.println("Doing " + reductions + " reductions...");

		for (int n = 0; n < reductions; n++) 
		{
			BufferedImage inputImage = CS450.getImageB();
			int width = inputImage.getWidth();
			int height = inputImage.getHeight();
			int newWidth = (int)((double)width * 0.5);
			int newHeight = (int)((double)height * 0.5);

			BufferedImage outputImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
			WritableRaster in = inputImage.getRaster();
			WritableRaster out = outputImage.getRaster();

			for (int y = 0; y < newHeight; y++)
			{
				for (int x = 0; x < newWidth; x++)
				{
					int oldX = (int)((double)x / 0.5);
					int oldY = (int)((double)y / 0.5);

					int oldX2 = oldX + 1;
					int oldY2 = oldY + 1;

					oldX = oldX < 0 ? 0 : oldX;
					oldY = oldY < 0 ? 0 : oldY;
					oldX2 = oldX2 > width - 1 ? width - 1 : oldX2;
					oldY2 = oldY2 > height - 1 ? height - 1 : oldY2;
					
					double p1 = (double)in.getSample(oldX, oldY, 0);
					double p2 = (double)in.getSample(oldX2, oldY, 0);
					double p3 = (double)in.getSample(oldX2, oldY2, 0);
					double p4 = (double)in.getSample(oldX, oldY2, 0);

					double localX = 0.5;
					double localY = 0.5;

					int val = (int)(p1*(1-localX)*(1-localY) + 
									p2*(1-localY)*localX + 
									p3*localX*localY + 
									p4*(1-localX)*localY);
				
					out.setSample(x, y, 0, val);
					out.setSample(x, y, 1, val);
					out.setSample(x, y, 2, val);
				}
			}
			
			CS450.setImageB(outputImage);
		}
	}

	public void doRotate() {
		String tStr = CS450.prompt("Angle:");
		double theta = Double.parseDouble(tStr);
		theta *=  Math.PI / 180.0;

		BufferedImage inputImage = CS450.getImageA();
		int width = inputImage.getWidth();
		int height = inputImage.getHeight();

		double cx = width / 2;
		double cy = height / 2;

		BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		WritableRaster in = inputImage.getRaster();
		WritableRaster out = outputImage.getRaster();

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				double xP = ((double)x - cx) * Math.cos(theta) - ((double)y - cy) * Math.sin(theta) + cx;
				double yP = ((double)x - cx) * Math.sin(theta) + ((double)y - cy) * Math.cos(theta) + cy;
		
				if (!(xP >= 0 && xP < width && yP >= 0 && yP < height)) {
					continue;
				}

				int floorX = (int)Math.floor(xP);
				int ceilX = (int)Math.ceil(xP);
				int floorY = (int)Math.floor(yP);
				int ceilY = (int)Math.ceil(yP);

				floorX = floorX < 0 ? 0 : floorX;
				ceilX = ceilX > width - 1 ? width - 1 : ceilX;
				floorY = floorY < 0 ? 0 : floorY;
				ceilY = ceilY > height - 1 ? height - 1 : ceilY;

				// System.out.println("[ " + floorX + ", " + ceilX + " ][ " + floorY + ", " + ceilY + " ]");

				double p1 = (double)in.getSample(floorX, floorY, 0);
				double p2 = (double)in.getSample(ceilX, floorY, 0);
				double p3 = (double)in.getSample(ceilX, ceilY, 0);
				double p4 = (double)in.getSample(floorX, ceilY, 0);

				double localX = xP - floorX;
				double localY = yP - floorY;

				int val = (int)(p1*(1-localX)*(1-localY) + 
								p2*(1-localY)*localX + 
								p3*localX*localY + 
								p4*(1-localX)*localY);	
				
				out.setSample(x, y, 0, val);
				out.setSample(x, y, 1, val);
				out.setSample(x, y, 2, val);
			}
		}
		
		CS450.setImageB(outputImage);
	}

	public void doPredictive_Encoding() 
	{
		BufferedImage inputImage = CS450.getImageA();
		int width = inputImage.getWidth();
		int height = inputImage.getHeight();

		BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		WritableRaster in = inputImage.getRaster();
		WritableRaster out = outputImage.getRaster();

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				double count = 0.0;
				double value = 0.0;

				if (pointInBounds(x - 1, y - 1, width, height))
				{
					value += in.getSample(x - 1, y - 1, 0);
					count++;
				}

				if (pointInBounds(x, y - 1, width, height)) 
				{
					value += in.getSample(x, y - 1, 0);
					count++;
				}

				if (pointInBounds(x + 1, y - 1, width, height))
				{
					value += in.getSample(x + 1, y - 1, 0);
					count++;
				}

				if (pointInBounds(x - 1, y, width, height)) 
				{
					value += in.getSample(x - 1, y, 0);
					count++;
				}

				int predValue = 0;
				if (count < 1.0) 
				{
					predValue = in.getSample(x, y, 0);
					System.out.println("Input at " + x +", "+ y + ": " + predValue);
				}
				else
				{
					value = value / count;
					predValue = (int)(value + 0.5);
				}

				// Calculate Error -  This ranges from [-P, 256-P], where P
				// is the predicted value. 
				// 
				// if r < 0
				// 		r = r + 256
				// 	else
				// 		do nothing
				// 		
				// 	DECOMPRESS:
				// 	
				// 	Actual = R + Prediction
				// 	
				// 	if A > 256 
				// 		A = A - 256
				// 
				int residual = 0;
				if (count < 1.0) 
				{
					residual = predValue;
				} 
				else 
				{
					residual = in.getSample(x, y, 0) - predValue;
				}

				if (residual < 0)
				{
					residual += 256;
				}

				out.setSample(x, y, 0, residual);
				if (x ==0 && y == 0) 
				{
					System.out.println("Out value: " + out.getSample(x, y, 0));
				}
			}
		}
		
		CS450.setImageB(outputImage);
	}

	public void doPredictive_Decoding()
	{
		BufferedImage inputImage = CS450.getImageA();
		int width = inputImage.getWidth();
		int height = inputImage.getHeight();

		BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		WritableRaster in = inputImage.getRaster();
		WritableRaster out = outputImage.getRaster();

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				double count = 0.0;
				double value = 0.0;

				if (pointInBounds(x - 1, y - 1, width, height))
				{
					value += out.getSample(x - 1, y - 1, 0);
					count++;
				}

				if (pointInBounds(x, y - 1, width, height)) 
				{
					value += out.getSample(x, y - 1, 0);
					count++;
				}

				if (pointInBounds(x + 1, y - 1, width, height))
				{
					value += out.getSample(x + 1, y - 1, 0);
					count++;
				}

				if (pointInBounds(x - 1, y, width, height)) 
				{
					value += out.getSample(x - 1, y, 0);
					count++;
				}

				int predValue = 0;
				if (count > 0.0)
				{
					value = value / count;
					predValue = (int)(value + 0.5);
				}

				// Calculate Error -  This ranges from [-P, 256-P], where P
				// is the predicted value. 
				// 
				// if r < 0
				// 		r = r + 256
				// 	else
				// 		do nothing
				// 		
				// 	DECOMPRESS:
				// 	
				// 	Actual = R + Prediction
				// 	 
				// 	if A > 256 
				// 		A = A - 256
				// 
				int actual = 0;
				if (count > 0.0) 
				{
					actual = in.getSample(x, y, 0) + predValue;
				} 
				else
				{
					System.out.println("Getting input image at " + x + ", " + y);
					actual = in.getSample(x, y, 0);
					System.out.println("Value: " + actual);
				}

				if (actual > 256)
				{
					actual -= 256;
				}

				out.setSample(x, y, 0, actual);
			}
		}
		
		CS450.setImageB(outputImage);
	}

	public void doSave()
	{
		BufferedImage img = CS450.getImageB();
		
		CS450.saveImage(img);
	}

	/**
	 * This is a private function that calcuates the histogram for
	 * image A. It returns a reference to the histogram array, which can
	 * then be used elsewhere for equalization or display.
	 */
	private int[] getHistogramData(BufferedImage inputImage) {
		int width = inputImage.getWidth();
		int height = inputImage.getHeight();
		
		WritableRaster in = inputImage.getRaster();

		int[] histogram = new int[HISTOGRAM_LENGTH];
		for (int i=0; i<256; i++) 
		{
			histogram[i] = 0;
		}

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				int val = in.getSample(x, y, 0);
				histogram[val]++;
			}
		}

		return histogram;
	}

	/**
	 * Private helper function to dump an array to stdout
	 */
	private void dumpArray(int[] data, String label)
	{
		System.out.print(label + " = [ ");
		for (int i=0; i<data.length; i++) 
		{
			System.out.print(data[i] + " ");
		}
		System.out.println("];");	
	}

	private void dumpArray(double[] data, String label)
	{
		System.out.print(label + " = [ ");
		for (int i=0; i<data.length; i++) 
		{
			System.out.print(data[i] + " ");
		}
		System.out.println("];");	
	}

	private double getMean(WritableRaster in, int x, int y, int width, int height, int gridSize)
	{
		double pixelCount = 0;
		double total = 0;

		x -= gridSize;
		y -= gridSize;

		int cx = x;
		int cy = y;

		for (int xOff=0; xOff<2*gridSize+1; xOff++)
		{
			for (int yOff=0; yOff<2*gridSize+1; yOff++)
			{
				cx = x + xOff;
				cy = y + yOff;

				if (cx < 0 || cy < 0 || cx > width - 1 || cy > height - 1)
					continue;

				total += in.getSample(cx, cy, 0);
				pixelCount++;
			}
		}

		return total / pixelCount;
	}

	private double getMedian(WritableRaster in, int x, int y, int width, int height, int gridSize)
	{
		x -= gridSize;
		y -= gridSize;

		int cx = x;
		int cy = y;

		ArrayList<Integer> candidates = new ArrayList<Integer>();

		for (int xOff=0; xOff<2*gridSize+1; xOff++)
		{
			for (int yOff=0; yOff<2*gridSize+1; yOff++)
			{
				cx = x + xOff;
				cy = y + yOff;

				if (cx < 0 || cy < 0 || cx > width - 1 || cy > height - 1)
					continue;

				candidates.add(in.getSample(cx, cy, 0));
			}
		}

		Collections.sort(candidates);
		int middle = (int)(candidates.size() / 2);

		if (candidates.size() % 2 == 1)
		{
			return candidates.get(middle);
		} 
		else
		{
			return (candidates.get(middle) + candidates.get(middle + 1)) / 2;
		}
	}

	private double getSobelX(WritableRaster in, int x, int y) {
		double total = 0.0;
		total += (getSample(in, x+1, y-1, 0) * 1 + getSample(in, x-1, y-1, 0) * -1);
		total += (getSample(in, x+1, y, 0) * 2 + getSample(in, x-1, y, 0) * -2);
		total += (getSample(in, x+1, y+1, 0) * 1 + getSample(in, x-1, y+1, 0) * -1);
		return total;
	}

	private double getSobelY(WritableRaster in, int x, int y) {
		double total = 0.0;
		total += (getSample(in, x-1, y+1, 0) * 1 + getSample(in, x-1, y-1, 0) * -1);
		total += (getSample(in, x, y+1, 0) * 2 + getSample(in, x, y-1, 0) * -2);
		total += (getSample(in, x+1, y+1, 0) * 1 + getSample(in, x+1, y-1, 0) * -1);
		return total;
	}

	private double getLaplacian(WritableRaster in, int x, int y) {
		double total = 0.0;
		total += (getSample(in, x-1, y, 0) * 1) + 
				 (getSample(in, x+1, y, 0) * 1) +
				 (getSample(in, x, y-1, 0) * 1) + 
				 (getSample(in, x, y+1, 0) * 1) + 
				 (getSample(in, x, y, 0) * -4);

		return total;
	}

	private int getSample(WritableRaster in, int x, int y, int channel) {
		try {
			return in.getSample(x, y, channel);
		} catch (Exception e) {
			return 0;
		}
	}

	private boolean pointInBounds(int x, int y, int width, int height) {
		return x >= 0 && x < width && y >= 0 && y < height;
	}
}

class CS450
{
	static Object hw;
	static JFrame window = new JFrame();
	static JViewport viewA, viewB;
	static ButtonHandler buttonHandler = new ButtonHandler();
	static JFileChooser fileChooser = new JFileChooser(".");
	static BufferedImage[][] history = new BufferedImage[32][2];
	static int historyLength, historyIndex;
	
	public static void run(Object hw)
	{
		CS450.hw = hw;
		
		history[0][0] = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
		history[0][1] = history[0][0];
		
		setupSwingComponents();
	}
	
	private static void setupSwingComponents()
	{
		ImageIcon iconA = new ImageIcon(history[0][0]);
		ImageIcon iconB = new ImageIcon(history[0][1]);
		
		JScrollPane paneA = new JScrollPane(new JLabel(iconA));
		paneA.setBorder(new TitledBorder("Image A"));
		viewA = paneA.getViewport();
		viewA.setPreferredSize(new Dimension(512, 512));
		
		JScrollPane paneB = new JScrollPane(new JLabel(iconB));
		paneB.setBorder(new TitledBorder("Image B"));
		viewB = paneB.getViewport();
		viewB.setPreferredSize(new Dimension(512, 512));
		
		JButton buttonSwap = new JButton("Swap");
		buttonSwap.setPreferredSize(new Dimension(70, 25));
		buttonSwap.addActionListener(buttonHandler);
		
		JButton buttonUndo = new JButton("Undo");
		buttonUndo.setPreferredSize(new Dimension(70, 25));
		buttonUndo.addActionListener(buttonHandler);
		
		JPanel controlPanel = new JPanel();
		controlPanel.add(buttonUndo);
		controlPanel.add(buttonSwap);
		controlPanel.setPreferredSize(new Dimension(70, 100));
		
		JPanel imagesPanel = new JPanel();
		imagesPanel.add(paneA);
		imagesPanel.add(controlPanel);
		imagesPanel.add(paneB);
		
		JPanel buttonPanel = reflectButtons();
		
		window.add(imagesPanel, BorderLayout.CENTER);
		window.add(buttonPanel, BorderLayout.SOUTH);
		window.pack();
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);
	}
	
	private static JPanel reflectButtons()
	{
		JPanel panel = new JPanel();
		
		for (Method method : hw.getClass().getMethods())
		{
			String name = method.getName().replace('_', ' ');
			
			if (name.startsWith("do"))
			{
				JButton button = new JButton(name.substring(2));
				button.addActionListener(buttonHandler);
				buttonHandler.map.put(name, method);
				panel.add(button);
			}
		}
		
		return panel;
	}
	
	public static BufferedImage getImageA()
	{
		return history[historyIndex][0];
	}
	
	public static BufferedImage getImageB()
	{
		return history[historyIndex][1];
	}
	
	private static void update()
	{
		JLabel label = (JLabel) viewA.getComponent(0);
		ImageIcon icon = (ImageIcon) label.getIcon();
		icon.setImage(history[historyIndex][0]);
		viewA.remove(0);
		viewA.add(label);
		
		label = (JLabel) viewB.getComponent(0);
		icon = (ImageIcon) label.getIcon();
		icon.setImage(history[historyIndex][1]);
		viewB.remove(0);
		viewB.add(label);
		
		window.repaint();
	}
	
	public static void setImageA(BufferedImage img)
	{
		int prevIndex = historyIndex;
		historyIndex++;
		historyIndex %= history.length;
		history[historyIndex][0] = img;
		history[historyIndex][1] = history[prevIndex][1];
		historyLength++;
		
		if (historyLength > history.length)
		{
			historyLength = history.length;
		}
		
		update();
	}
	
	public static void setImageB(BufferedImage img)
	{
		int prevIndex = historyIndex;
		historyIndex++;
		historyIndex %= history.length;
		history[historyIndex][0] = history[prevIndex][0];
		history[historyIndex][1] = img;
		historyLength++;
		
		if (historyLength > history.length)
		{
			historyLength = history.length;
		}
		
		update();
	}
	
	static void swapImages()
	{
		BufferedImage img = history[historyIndex][0];
		history[historyIndex][0] = history[historyIndex][1];
		history[historyIndex][1] = img;
		
		update();
	}
	
	static void undo()
	{
		if (historyLength > 0)
		{
			historyLength--;
			historyIndex--;
			historyIndex += history.length;
			historyIndex %= history.length;
			
			update();
		}
	}
	
	public static BufferedImage openImage()
	{
		try
		{
			int val = fileChooser.showOpenDialog(window);
			
			if (val == JFileChooser.APPROVE_OPTION)
			{
				File file = fileChooser.getSelectedFile();
				
				BufferedImage img = ImageIO.read(file);
				
				if (img == null) throw new Exception("unable to read image");
				
				return img;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void saveImage(BufferedImage img)
	{
		try
		{
			int val = fileChooser.showSaveDialog(window);
			
			if (val == JFileChooser.APPROVE_OPTION)
			{
				File file = fileChooser.getSelectedFile();
				int dot = file.getName().lastIndexOf('.');
				String suffix = file.getName().substring(dot + 1);
				ImageWriter writer = ImageIO.getImageWritersBySuffix(suffix).next();
				ImageOutputStream out = ImageIO.createImageOutputStream(file);
				writer.setOutput(out);
				writer.write(img);
				out.close();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static String prompt(String message)
	{
		return prompt(message, null, null);
	}
	
	public static String prompt(String message, String defaultValue)
	{
		return prompt(message, null, defaultValue);
	}
	
	public static String prompt(String message, String[] choices)
	{
		return prompt(message, choices, null);
	}
	
	public static String prompt(String message, String[] choices, String defaultValue)
	{
		Object answer = JOptionPane.showInputDialog(
			window,
			message,
			null,
			JOptionPane.PLAIN_MESSAGE,
			null, // no icon
			choices,
			defaultValue
		);
		
		return (String) answer;
	}
}

class ButtonHandler implements ActionListener
{
	HashMap<String, Method> map = new HashMap<String, Method>();
	
	@Override
	public void actionPerformed(ActionEvent event)
	{
		JButton button = (JButton) event.getSource();
		String text = button.getText();
		
		if (text.equals("Swap"))
		{
			CS450.swapImages();
		}
		else if (text.equals("Undo"))
		{
			CS450.undo();
		}
		else try
		{
			map.get("do"+text).invoke(CS450.hw);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}