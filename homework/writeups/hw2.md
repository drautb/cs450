<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

Homework #2 Writeup
===================

Ben Draut / CS 450 / Jan. 13, 2014

## Original Image

Here we see the original image. The lack of contrast makes it difficult to discern any detail. We can see that the histogram is heavily condensed, with nearly every value falling beneath 50.

<div style="width:49%; float:left">
    <img src="../img/mystery.png" style="width:80%"/>
</div>
<div style="width:49%; float:right;">
    <img src="img/mystery-hist.png"/>
</div>
<div style="clear:both;"></div>

## Equalized Image (GIMP)

Here we can see the effects of histogram equalization. (Performed in GIMP) The histogram has been stretched out over the range of values, yielding a much higher contrast between different intensities in the image. A nature scene can be easily discerned.

<div style="width:49%; float:left">
    <img src="img/hw2-gimp-equalized.png" style="width:80%"/>
</div>
<div style="width:49%; float:right;">
    <img src="img/hw2-gimp-equalized-hist.png"/>
</div>
<div style="clear:both;"></div>

## Equalized Image (Me)

Here we can see that my implementation of histogram equalization matches the results achieved in GIMP. A previously indiscernible image has become clear due to increased contrast.

![](img/hw2-my-equalized.png)
 
<div style="width:49%; float:left;">
    <h3>Equalized Histogram</h3>
    <p>Here we see that the equalized histogram generated from my implementation matches the one obtained from GIMP.</p>
    <img src="img/hw2-my-equalized-hist.png"/>
</div>
<div style="width:49%; float:right">
    <h3>Lookup Table</h3>
    <p>This table shows the mappings that were calculated between intensity values. As we would expect, each of the lower values has been mapped to a much higher value, thus spacing out the intensities and increasing contrast. (Seeing this is where the equalization idea clicked for me.)</p>
    <img src="img/hw2-lookup-table.png"/>
</div>
<div style="clear:both;"></div>
