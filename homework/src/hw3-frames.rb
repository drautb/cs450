require 'pnm'

options = {
  :img_dir => "../img/Frames/",
  :img_prefix => "Cat",
  :img_suffix => ".pgm",
  :img_width => 200,
  :img_height => 247,
  :averages => [ 2, 5, 10, 20, 40 ]
}

puts "Homework #3 - Frame Averaging Assignment"
puts "Options: #{options}"

for n in options[:averages]
  puts "\nAveraging #{n} images..."

  imgPixels = []

  # Load in all the images
  for i in 0..(n-1)
    imgFile = "#{options[:img_dir]}#{options[:img_prefix]}#{i}#{options[:img_suffix]}"
    image = PNM.read(imgFile)
    imgPixels[i] = image.pixels
  end

  avgPixels = imgPixels[0]

  # Average the loaded images
  for h in 0..(options[:img_height]-1)
    for w in 0..(options[:img_width]-1)
      sum = 0
      for i in 0..(n-1)
        sum += imgPixels[i][h][w]
      end
      avg = sum / n
      avgPixels[h][w] = avg
    end
  end

  outputFile = "avg#{n}.pgm"
  puts "Saving '#{outputFile}'..."
  finalImg = PNM::Image.new(avgPixels)
  finalImg.write(options[:img_dir] + outputFile)
end
