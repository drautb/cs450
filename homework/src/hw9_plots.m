%% Fourier Transform V1 of v1.
%% Play sounds with `sound(data, 44100)` or `soundsc(data, 44100)` if using integer values.

[v1 fs] = audioread('data/v1.wav');

% file = fopen('data/v1.raw');
% v1 = fread(file, Inf, 'int16=>double');
% fclose(file);

% file = fopen('data/v1-8bit.raw');
% v1 = fread(file, Inf, 'int8=>double');
% fclose(file);

% Plot the original signal
figure
figureHandle = gcf;
plot(v1);
grid on;
title('v1 - Original Signal');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw9/figure_1.png', '-dpng');
close(figureHandle);

% Take the fourier transform
t = fft(v1);
V1 = sqrt(real(t).^2 + imag(t).^2);

% Plot the fourier transform
figure
figureHandle = gcf;
plot(V1);
grid on;
title('V1 - Fourier Transform');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw9/figure_2.png', '-dpng');
close(figureHandle);

% Grab the highest frequency with non-zero magnitude
for a = 1:(size(V1) / 2.0)
	if mean(V1(a:a+10)) < 0.025
		disp(a+5);
		disp(V1(a+5));
		break;
	end
end	

% Take the fourier transform
V1pow = V1.^2;

% Plot the power spectrum
figure
figureHandle = gcf;
plot(V1pow);
grid on;
title('|V1|^2 - Power Spectrum');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw9/figure_3.png', '-dpng');
close(figureHandle);

% Get the 'ooh' sound part
ooh = v1(22385:31339);

% Plot the ooh sound
figure
figureHandle = gcf;
plot(ooh);
grid on;
title('Voweled Sound');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw9/figure_4.png', '-dpng');
close(figureHandle);

% Butterworth filtering
V1 = fft(v1);
n = 1;
u = [1:size(v1)];
uc = 46602;
sound(real(ifft((1./(1+((u.^2)./(uc^2)).^n))'.*V1)), fs);

% Plot final power spectrum and filter
n = 1;
u = [1:size(v1)];
uc = 20602;
h = 1./(1+((u.^2)./(uc^2)).^n);

figure
figureHandle = gcf;
plot(h);
grid on;
title('Butterworth Filter');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw9/figure_5.png', '-dpng');
close(figureHandle);

t = fft(v1);
V1 = sqrt(real(t).^2 + imag(t).^2);
powSpec = (h'.*V1).^2;

figure
figureHandle = gcf;
plot(powSpec);
grid on;
title('Filtered Power Spectrum - |V1|^2');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw9/figure_6.png', '-dpng');
close(figureHandle);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% V2

[v2 fs] = audioread('data/v2.wav');

% file = fopen('data/v2.raw');
% v2 = fread(file, Inf, 'int16=>double');
% fclose(file);

% file = fopen('data/v2-8bit.raw');
% v2 = fread(file, Inf, 'int8=>double');
% fclose(file);

% Plot the original signal
figure
figureHandle = gcf;
plot(v2);
grid on;
title('v2 - Original Signal');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw9/figure_7.png', '-dpng');
close(figureHandle);

% Take the fourier transform
t = fft(v2);
V2 = sqrt(real(t).^2 + imag(t).^2);

% Plot the fourier transform
figure
figureHandle = gcf;
plot(V2);
grid on;
title('V2 - Fourier Transform');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw9/figure_8.png', '-dpng');
close(figureHandle);

% Grab the highest frequency with non-zero magnitude
for a = 1:(size(V2) / 2.0)
	if mean(V2(a:a+10)) < 0.025
		disp(a+5);
		disp(V2(a+5));
		break;
	end
end	

% Take the fourier transform
V1pow = V2.^2;

% Plot the power spectrum
figure
figureHandle = gcf;
plot(V1pow);
grid on;
title('|V2|^2 - Power Spectrum');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw9/figure_9.png', '-dpng');
close(figureHandle);

% Get the 'wan' sound part
begin = size(v2) * (1.57 / 5.472);
endpt = size(v2) * (1.71 / 5.472);
ooh = v2(begin:endpt);

% Plot the wan sound
figure
figureHandle = gcf;
plot(ooh);
grid on;
title('Voweled Sound');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw9/figure_10.png', '-dpng');
close(figureHandle);

% Butterworth filtering
V2 = fft(v2);
n = 1;
u = [1:size(v2)];
uc = 38822;
sound(real(ifft((1./(1+((u.^2)./(uc^2)).^n))'.*V2)), fs);

% Plot final power spectrum and filter
n = 1;
u = [1:size(v2)];
uc = 20602;
h = 1./(1+((u.^2)./(uc^2)).^n);

figure
figureHandle = gcf;
plot(h);
grid on;
title('Butterworth Filter');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw9/figure_11.png', '-dpng');
close(figureHandle);

t = fft(v2);
V2 = sqrt(real(t).^2 + imag(t).^2);
powSpec = (h'.*V2).^2;

figure
figureHandle = gcf;
plot(powSpec);
grid on;
title('Filtered Power Spectrum - |V2|^2');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw9/figure_12.png', '-dpng');
close(figureHandle);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% V3

[v3 fs] = audioread('data/v3.wav');

% Take the fourier transform
t = fft(v3);
V3 = sqrt(real(t).^2 + imag(t).^2);

% Butterworth filtering
V3 = fft(v3);
n = 1;
u = [1:size(v3)];
uc = (20602 + 18822) / 2.0;
sound(real(ifft((1./(1+((u.^2 )./(uc^2)).^n))'.*V3)), fs);

