Homework #9 Writeup
===================

Ben Draut / CS 450 / Mar. 31, 2014

## V1

**1) The first speech waveform is $v1$. Listen to the .wav file using any audio player and see if you can recognize the person behind the voice. Who do you think it is?**

Elder Oaks

**2) Compute the Fourier Transform, $V1$, of $v1$ and report the highest nonzero frequency found in the spectrum.**

Original Signal - $v1$

![](../img/hw9/figure_1.png)

Fourier Transform - $V1$

![](../img/hw9/figure_2.png)

The highest frequency with non-zero magnitude was 46602. 

**Compute the sampling rate. Does it appear that the signal was digitized at a sufficiently high (i.e. Nyquist) rate? Why or why not?**

Length of signal: $3.452018s$

Total samples in signal: $152,234$

Sampling Rate: $ \frac{152234}{3.452018} = 44100.0018$ samples/sec or about $44100 Hz$.

It does not appear that this sample was digitized at a sufficiently high rate. The highest frequency non-zero frequency was $46602 Hz$. Twice this is $93204 Hz$, but the sampling rate of the sample was only $44100 Hz$.

**Plot the Power Spectrum, $|V1|^2$ of the entire waveform, $v1$. Also plot a .5 sec portion of a voweled sound in $v1$. Indicate which voweled sound it is.  Include a copy of the plots in your write-up.**

Power Spectrum - $|V1|^2$

![](../img/hw9/figure_3.png)

Vowel Sound - This is the 'ooh' sound when Elder Oaks says 'Who.'

![](../img/hw9/figure_4.png)

As we also discussed in class, the voweled sound exhibits a very regular periodic nature, while other sounds appear more noisy.

**3) When transmitting signals (over cell phones, for example), it is good to use as little bandwidth as necessary. To find out how much bandwidth is necessary, low-Pass Filter $V1$ in the frequency domain – using the Butterworth filter - to produce a filtered spectrum $V1’$.** 

$H(u) = \frac{1}{1\:+(\frac{u^2}{u^2_c})^{^{^n}}\:}$    where  $u_c$  = cutoff frequency

**Begin with $n=1$ and $u_c$ set to the highest nonzero frequency. Then gradually decrease $u_c$, compute the inverse transform, and listen to the resulting $v1’$. Repeat this until you detect some perceptible loss in the filtered speech. This loss could be a loss of clarity, loss of volume or any difference from the original. Experiment with $n$ to altar the steepness of the cutoff to see if increasing $n$ will allow you to further decrease $u_c$.**

With $n=1$ and an initial cutoff frequency of $u_c = 46602$, I was able to decrease $u_c$ to $20602$ before I could discern a change. The sample sounded to be at about the same volume, but the voice was starting to sound muffled.

Increasing $n$ at all seemed to reduce the clarity of the voice as well, I was unable to get any improvement by increasing it.

Final values: $n = 1, u_c = 20602$. This means that we would need $2u_c$, or $41204 Hz$ of bandwidth for this signal.

![](../img/hw9/figure_5.png)

![](../img/hw9/figure_6.png)


## V2

**Compute the Fourier Transform, $V1$, of $v1$ and report the highest nonzero frequency found in the spectrum.**

Original Signal - $v2$

![](../img/hw9/figure_7.png)

Fourier Transform - $V2$

![](../img/hw9/figure_8.png)

Highest frequency: $38822$

**Compute the sampling rate. Does it appear that the signal was digitized at a sufficiently high (i.e. Nyquist) rate? Why or why not?**

Length of signal: $5.472s$

Total samples in signal: $87,552$

Sampling Rate: $ \frac{87552}{5.472} = 16,000$ samples/sec or about $16000 Hz$.

Again, this appears to be insufficient. The highest frequency was $38822 Hz$, which doubled is $77644 Hz$. The sampling frequency was only $16000 Hz$.

**Plot the Power Spectrum, $|V2|^2$ of the entire waveform, $v2$. Also plot a .5 sec portion of a voweled sound in $v2$. Indicate which voweled sound it is.  Include a copy of the plots in your write-up.**

Power Spectrum - $|V2|^2$

![](../img/hw9/figure_9.png)

Vowel Sound - This is the 'aah' sound when Leia says 'Obi Wan'.

![](../img/hw9/figure_10.png)

As we also discussed in class, the voweled sound exhibits a very regular periodic nature, while other sounds appear more noisy. The 'aah' sound is more jagged than the 'ooh' sound however.

**Filtering: Begin with $n=1$ and $u_c$ set to the highest nonzero frequency. Then gradually decrease $u_c$, compute the inverse transform, and listen to the resulting $v2’$. Repeat this until you detect some perceptible loss in the filtered speech. This loss could be a loss of clarity, loss of volume or any difference from the original. Experiment with $n$ to altar the steepness of the cutoff to see if increasing $n$ will allow you to further decrease $u_c$.**

With $n=1$ and an initial cutoff frequency of $u_c = 38822$, I was able to decrease $u_c$ to $18822$ before I could discern a change. Again, the sample volume sounded about the same, but the voice sounded suppressed or muffled. 

Increasing $n$ at all seemed to reduce the clarity of the voice as well, I was unable to get any improvement by increasing it. I noticed that it made each sound sound a bit crisper, but overall the voice was more grainy.

Final values: $n = 1, u_c = 18822$. This means we would need $2u_c$, or $37644 Hz$ of bandwidth for this signal.

![](../img/hw9/figure_11.png)

![](../img/hw9/figure_12.png)


## V3

**Now, using the average values for $n$ and $u_c$, (found for $v1$ and $v2$), apply your Butterworth Filter to one last signal, $v3$, before actually listening to $v3$. You will, of course, need to compute $V3$ first.**

```matlab
[v3 fs] = audioread('data/v3.wav');

% Take the fourier transform
t = fft(v3);
V3 = sqrt(real(t).^2 + imag(t).^2);

% Butterworth filtering
V3 = fft(v3);
n = 1;
u = [1:size(v3)];
uc = (20602 + 18822) / 2.0;
sound(real(ifft((1./(1+((u.^2)./(uc^2)).^n))'.*V3)), fs);
```

**Write down what you think is being said in $v3’$. Then write down what is being said by listening to $v3$.**

$v3'$: "Insanity runs in my family...it practically gallops!" (Arsenic and Old Lace)

$v3$: "Insanity runs in my family...it practically gallops!"

**What can you conclude about the bandwidth needed to record and transmit speech in general, and $v3$ in particular?**

In general, it looks like human speech only requires around $20000 Hz$ of bandwidth. Anything more than that may add a little, but not too much.

A lot of these answers didn't make a lot of sense to me, so I kept doing some digging. First, when I mathematically determined that $v1$ and $v2$ weren't sampled at a sufficiently high rate, that included all frequencies, even those that I couldn't hear. As I filtered out the higher frequencies, I realized that the actual sampling rate was sufficient to carry enough data.

It was also interesting to read that the human voice only needs about $4000 Hz$, which means that it only needs $8000 Hz$ of bandwidth. This made a lot of sense, because as I experimented with the filtering, I had to drop $u_c$ under a few thousand in order to have a crippling effect on the sound. Cool! 

