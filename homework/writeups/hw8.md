<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
table {
	width: 100%;
}
</style>

Homework #8 Writeup
===================

Ben Draut / CS 450 / Mar. 28, 2014

##

**1) Write a program that reads in an image and magnifies it by a specified (integer) factor using bilinear interpolation. That is, if the image you read in is $N$ by $M$, and the magnification factor is $f$, the resulting image will be $Nf$ by $Mf$. Test your program using parrots.pgm and factors $f = 2$ and $f = 3$.**

Original Image:

![](../img/hw8/parrots.png)

$f=2$: 

![](../img/hw8/parrots-2x.png)

$f=3$:

![](../img/hw8/parrots-3x.png)


##

**2) Write a program that reads in an image and reduces it by a specified (integer) factor. That is, if the image you read in is $N$ by $M$, and the magnification factor is $f$, the resulting image will be $N/f$ by $M/f$ (round these if necessary). Test your program using parrots.pgm and the images in the provided (imageTest2.pgm, imageTest3.pgm, imageTest4.pgm). Use reduction factors of $f = 4$ and $f = 8$. Is there anything you have to do differently from Part 1 in order to make this work well? (Think about the sampling theorem.)**

<table>
	<tr>
		<th>Original Image</th>
		<th>$f=4$</th>
		<th>$f=8$</th>
	</tr>
	<tr>
		<td><img src="../img/hw8/parrots.png"/></td>
		<td><img src="../img/hw8/parrots-4.png"/></td>
		<td><img src="../img/hw8/parrots-8.png"/></td>
	</tr>
	<tr>
		<td><img src="../img/hw8/2.png"/></td>
		<td><img src="../img/hw8/2-4.png"/></td>
		<td><img src="../img/hw8/2-8.png"/></td>
	</tr>
	<tr>
		<td><img src="../img/hw8/3.png"/></td>
		<td><img src="../img/hw8/3-4.png"/></td>
		<td><img src="../img/hw8/3-8.png"/></td>
	</tr>
	<tr>
		<td><img src="../img/hw8/4.png"/></td>
		<td><img src="../img/hw8/4-4.png"/></td>
		<td><img src="../img/hw8/4-8.png"/></td>
	</tr>
</table>

This was pretty cool. When we're doing backwards mapping to reduce an image with bilinear interpolation, we can only sample 4 pixels to interpolate with. However, in an image that is reduced by a factor of $n$, there are $n^2$ pixels from the old image being represented by each pixel in the new image. In order to produce a high-quality reduction, I had to iteratively reduce my image, each time by a factor of 2, in order to make sure each pixel contributes to the final result.

##

**3) Write a program that rotates an image around its center by a specified angle. The transformation for rotation by angle $\theta$ around point $(x_c, y_c)$ is**

$x' = (x - x_c)\cos(\theta) - (y - y_c)\sin(\theta) + x_c$

$y' = (x - x_c)\sin(\theta) + (y - y_c)\cos(\theta) + y_c$

**Use the backwards warping algorithm and bilinear interpolation as discussed in class. You may choose any image you wish to test and demonstrate your code.**

<table>
	<tr>
		<th>Original Image</th>
		<th>Rotated 30 Degrees</th>
	</tr>
	<tr>
		<td><img src="../img/hw8/parrots.png"/></td>
		<td><img src="../img/hw8/parrots-rot30.png"/></td>
	</tr>
</table>

**Apply your method to the image in 15 degree increments to rotate the image by 120 degrees. Compare this to rotating directly by 120 degrees. What happens? Why?**

<table>
	<tr>
		<th>Rotated 15 Degrees 8 Times</th>
		<th>Rotated 120 Degrees</th>
	</tr>
	<tr>
		<td><img src="../img/hw8/parrots-rot15.png"/></td>
		<td><img src="../img/hw8/parrots-rot120.png"/></td>
	</tr>
</table>

The image that was rotated just once is still very clear compared to the other. When we rotate images this way, we lose information with each rotation. Each rotation performs bilinear interpolation which causes us to lose a bit of the original data. When we rotate the image 8 times, this effect is amplified 8 times, thus yielding a much fuzzier picture.


