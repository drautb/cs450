<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

<style type="text/css">
img {
	max-width: 450px important!;
}
</style>

Homework #5 Writeup - Fourier Transform
=======================================

Ben Draut / CS 450 / Feb. 18, 2014

## Simple Sines and Cosines

**Generate and display a one-dimensional sine wave with 128 samples that makes 8 cycles over this length:**

$f(t) = \sin(2\pi s t / N)$ where $s = 8$ and $N = 128$.

Plot: ![](../img/hw5/figure_1.png)

**Real Part:** ![](../img/hw5/figure_2.png)

For this function, the real part is approximately zero in every case. This makes sense because the real part of the transform tells us how much of a cosine of frequency $u$ was in the original signal. Since the original signal was a sine function, this should be zero.

**Imaginary Part:** ![](../img/hw5/figure_3.png)

The imaginary part represents how much of a sine function of frequency $u$ was in the original signal. Since The original signal was a sine function, the imaginary part has spikes at the frequency of the original function. ($s=8$)

**Magnitude:** ![](../img/hw5/figure_4.png)

The magnitude spikes to 1 when $u=8$, which is again to be expected because the amplitude of our original signal was $1$.

**Phase:** ![](../img/hw5/figure_5.png)

The phase values look pretty erratic, but the value of the phase at $u=8$ is $-\pi/2$, which makes sense because this is a sine wave. A cosine wave shifted any direction by $\pi/2$ becomes a sine wave. If this were a cosine wave, we would expect the phase value at $u=8$ to be 0.

##

**Generate and display a one-dimensional cosine wave with 128 samples that makes 8 cycles over this length:**

$f(t) = \cos(2\pi s t / N)$ where $s = 8$ and $N = 128$.

**Plot:** ![](../img/hw5/figure_6.png)

**Real Part:** ![](../img/hw5/figure_7.png)

Here we see that the real part has a spike at $s=8$, which corresponds to the frequency of the original signal. This makes sense again because the real part describes the cosine component of the frequency.

**Imaginary Part:** ![](../img/hw5/figure_8.png)

Here the imaginary part is approximately zero, again making sense because there is no sine component in the original signal.

**Magnitude:** ![](../img/hw5/figure_9.png)

This shows us that the magnitude of the original signal was 1 when $s=8$, which again corresponds.

**Phase:** ![](../img/hw5/figure_10.png)

Here the phase values look erratic again. However, the phase value at $u=8$ is 0, which makes sense because this is a cosine wave, so the phase shift should be zero.

##

**Generate and display the sum of a one-dimensional sine wave  and a one-dimensional cosine wave with 128 samples that makes 8 cycles over this length:**

$f(t) = \sin(2\pi s t / N) + \cos(2\pi s t / N)$ where $s = 8$ and $N = 128$.

**Plot:** ![](../img/hw5/figure_11.png)

**Real Part:** ![](../img/hw5/figure_12.png)

Here we can see that the real part spikes, showing that there is a cosine component signal when $u=8$.

**Imaginary Part:** ![](../img/hw5/figure_13.png)

Here we can see that the imaginary part spikes, showing that there is a sine component signal when $u=8$.

**Magnitude:** ![](../img/hw5/figure_14.png)

Here we can see that the magnitude of the signal when $u=8$ approaches $1.5$ this is consistent with the original plot in which the sum of the two functions approaches this amplitude as well.

**Phase:** ![](../img/hw5/figure_15.png)

We know that both the sine and cosine wave of this function had a frequency of 8. If we look at the phase value when $u=8$, we find $\pi/4$, which makes sense because the shift for the combination of a sine and cosine wave should be between the two, between 0 and $\pi/2$.

##

**Play with the relative weightings of the sine and cosine parts and see what happens. Report your findings in your write-up.**

This was interesting, I could see the effect of weighting each function in the sum. The results more closely resembled the results of the individual function that was weighted more heavily.

## Rect Function

**Compute the Fourier Transform of the Rectangular Pulse and plot the magnitude and Power Spectrum.**

**Plot:** ![](../img/hw5/figure_16.png)

**Magnitude:** ![](../img/hw5/figure_17.png)

**Power Spectrum:** ![](../img/hw5/figure_18.png)

We know that the magnitude for a box function looks like this:

$|F(\mu)| = AT| \frac{\sin(\pi \mu W)}{(\pi \mu W)}|$

This has the effect that the height of the curves goes to zero as the frequency goes to $\pm\infty$, and the height of the curve at $u=0$ is $AW$, which is consistent with the diagram.

## Gaussian Function

**Compute the Fourier Transform of the Gaussian and plot the magnitude and Power Spectrum.**

**Plot:** ![](../img/hw5/figure_19.png)

**Magnitude:** ![](../img/hw5/figure_20.png)

**Power Spectrum:** ![](../img/hw5/figure_21.png)

For a Gaussian function, $F(u) = e^{-\pi u^2}$.

## Frequency Analysis

**Plot:** ![](../img/hw5/figure_22.png)

Most Significant Frequencies:

Frequency: 0		Magnitude: 199.9921875

Frequency: 8		Magnitude: 33.381388655235156

Frequency: 16		Magnitude: 33.37664880866268

## Discover the Transfer Function

**$f(t)$** ![](../img/hw5/figure_23.png)

**$g(t)$** ![](../img/hw5/figure_24.png)

We know that $f(t) * h$ results in $g(t)$. By the convolution theorem, we know that $FH = G$, or the product of the Fourier transforms of $f$ and $h$ is equal to the Fourier transform of $g$. Therefore:

$H = \frac{G}{F}$

and:

$h = \mathfrak{F^{-1}}H$

**$|H(u)|$:** ![](../img/hw5/figure_25.png)

**$h(t)$:** ![](../img/hw5/figure_26.png)

As we would expect, the original function, $h(t)$ bears resemblance to the original input and output functions. 

