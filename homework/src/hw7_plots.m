%% 1-D Filtering
%% Plot original data
figure
figureHandle = gcf;
load 'data/1D_Noise.dat';
data = X1D_Noise;
N = length(data);
plot(data);
grid on;
title('1D\_Noise Data');
xlabel('u');
ylabel('magnitude');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw7/figure_1.png', '-dpng');
close(figureHandle);

%% Filter the data
d0 = 20; % Cutoff freq
filterData = data;
filtered = data;
for u = 1:N
	filterData(u) = exp((-u^2)/(2*d0^2));
	filtered(u) = data(u) * exp((-u^2)/(2*d0^2));
end

%% PLot the filter
figure
figureHandle = gcf;
plot(filterData);
grid on;
axis manual;
axis([0 100 0 1.4]);
title('Gaussian Filter');
xlabel('u');
ylabel('magnitude');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw7/figure_2.png', '-dpng');
close(figureHandle);

%% Plot the filtered data
figure
figureHandle = gcf;
plot(filtered);
grid on;
axis manual;
axis([0 100 0 1.4]);
title('1D\_Noise Data - Filtered');
xlabel('u');
ylabel('magnitude');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw7/figure_3.png', '-dpng');
close(figureHandle);

%% PART B - 2D Filtering

%% Save original images
figure;
fh = gcf;
imshow(imread('/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/2D_White_Box.png'));
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw7/figure_4.png', '-dpng');
close(fh);

figure;
fh = gcf;
imshow(imread('/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw4/avg9x9.png'));
print(fh, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw7/figure_5.png', '-dpng');
close(fh);

imageData = imread('/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/2D_White_Box.png');
kernel = ones(9, 9);
paddedDims = [104 104];
H_u = fft2(double(kernel), paddedDims(1), paddedDims(2));
F = fft2(double(imageData), paddedDims(1), paddedDims(2));
FDF = H_u .* F;
fdf = ifft2(FDF);
fdf = fdf(8:size(imageData, 1), 8:size(imageData, 2));

figure;
figureHandle = gcf;
imshow(fdf, []);
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw7/figure_6.png', '-dpng');
close(figureHandle);

imageData = imread('/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw7/interference.png');

figure;
figureHandle = gcf;
imshow(imageData);
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw7/figure_7.png', '-dpng');
close(figureHandle);

trans = fft2(imageData);
transShow = fftshift(trans);
transShow = abs(transShow);
transShow = log(transShow + 1);
transShow = mat2gray(transShow);

figure;
figureHandle = gcf;
imshow(transShow, []);
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw7/figure_8.png', '-dpng');
close(figureHandle);

% Modify the transform
modifiedTrans = trans;
mtReal = real(modifiedTrans);
mtImag = imag(modifiedTrans);
mag = sqrt(mtReal .^2 + mtImag .^2);
phase = atan2(mtImag, mtReal);

biggestDiff = 0;
biggestDiffX = 0;
biggestDiffY = 0;
biggestDiffAvg = 0;

% Find the most out of place freq
for x = 5:length(mag)
	for y = 5:length(mag)
		p = zeros(1,8);
		center = mag(x,y);
		total = 0;

		if x > 1 && y > 1
			p(1) = mag(x-1, y-1);
			total = total + 1;
		end

		if y > 1
			p(2) = mag(x, y-1);
			total = total + 1;
		end

		if x < length(mag) && y > 1
			p(3) = mag(x+1, y-1);
			total = total + 1;
		end

		if x > 1
			p(4) = mag(x-1, y);
			total = total + 1;
		end

		if x < length(mag)
			p(5) = mag(x+1, y);
			total = total + 1;
		end

		if x > 1 && y < length(mag)
			p(6) = mag(x-1, y+1);
			total = total + 1;
		end

		if y < length(mag)
			p(7) = mag(x, y+1);
			total = total + 1;
		end

		if x < length(mag) && y < length(mag)
			p(8) = mag(x+1, y+1);
			total = total + 1;
		end

		avg = sum(p) / total;

		thisDiff = center - avg;
		if thisDiff > biggestDiff
			disp(x);
			disp(y);
			disp(thisDiff);
			biggestDiff = thisDiff;
			biggestDiffAvg = avg;
			biggestDiffX = x;
			biggestDiffY = y;
		end
	end
end

mag(biggestDiffX, biggestDiffY) = biggestDiffAvg;
mag(length(mag) - biggestDiffX + 2, length(mag) - biggestDiffY + 2) = biggestDiffAvg;

% Recompute the new values
newReal = mag .* cos(phase);
newImag = mag .* sin(phase);
modifiedTrans = newReal + 1i*newImag;

transShow = ifftshift(modifiedTrans);
transShow = abs(transShow);
transShow = log(transShow + 1);
transShow = mat2gray(transShow);

figure;
figureHandle = gcf;
imshow(transShow, []);
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw7/figure_9.png', '-dpng');
close(figureHandle);

newImage = ifft2(modifiedTrans);

figure;
figureHandle = gcf;
imshow(uint8(real(newImage)), []);
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw7/figure_10.png', '-dpng');
close(figureHandle);
