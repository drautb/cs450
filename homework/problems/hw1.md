<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

Homework #1 Problems
====================

Ben Draut / CS 450 / Jan. 10, 2014

**1. A standard music CD holds 74 minutes of stereo (2-channel) audio at 44,100 samples/second and 16 bits/sample/channel. How many megabytes of raw data can such a CD hold?**

$74 \frac{minutes}{CD} * 60 \frac{seconds}{minute} * 44,100 \frac{samples}{second} * 16 \frac{bits}{sample} = 3,132,864,000 \frac{bits}{CD}$

Assuming 1 Megabyte contains 8,388,608 bits:

$\frac{3,132,864,000}{8,388,608} = 373.4665 MB$

Since the CD is stereo, we multiply this by 2 to get approximately 747 MB.

**2. 2.5 from textbook**

$\frac{Size of View}{500mm} = \frac{7mm}{35mm}$

$Size of View = 100mm$

$Line Width = \frac{7mm}{1024 Lines} = 0.00684 mm/line$

$Width of a Line Pair = 0.01367 mm/pair$

This camera can resolve $\frac{100mm}{0.01367 mm/pair} = 7314.286$ line pairs.

**3. 2.10 from textbook**

In an HDTV with 1125 horizontal interlaced lines and a 16:9 aspect ratio, the resolution is

$\frac{1125}{2} = 562.5$ pixels high and 

$\frac{w}{562.5} = \frac{16}{9}, w = 1000$ pixels wide.

Rounding up, this means that one frame contains $1000 * 563 = 563,000$ pixels.

Since each pixel requires 24 bits, every frame contains $563,000 * 24 = 13,512,000$ bits of data.

In a 2 hour movie playing at 30 fps, there are $30 * 60 * 60 * 2 = 216,000$ frames.

This yields $216,000 * 13,512,000 = 2,918,592,000,000$ bits for the entire film.

**4. 2.19 from textbook**

If the median operator is linear, then $med(a + b) \equiv med(a) + med(b)$.

We can disprove this by counterexample:

Let $a = [1,1,3]$ and $b = [1,2,0]$.

$med(a) = 1, med(b) = 1$, so $med(a) + med(b) = 2$

However, $med(a + b) = med([2,3,3]) = 3 \neq 2$.


