% #2 System

syms a b c d;
[sol_a, sol_b, sol_c, sol_d] = ...
	solve(2*a + 4*b + 8*c + d == 7, ...
		  2*a + 5*b + 10*c + d == 8, ...
		  3*a + 4*b + 12*c + d == 8, ...
		  3*a + 5*b + 15*c + d == 10)

v = 2.7*sol_a + 4.8*sol_b + 2.7*4.8*sol_c + sol_d;
v = double(v);
disp(v);


% #3 System

syms c_1 c_2 c_3 c_4 c_5 c_6 c_7 c_8;

[sol_c_1, sol_c_2, sol_c_3, sol_c_4] = ...
	solve(0*c_1 + 0*c_2 + (0*0)*c_3 + c_4 == 0, ...
		  1*c_1 + 0*c_2 + (0*1)*c_3 + c_4 == 1, ...
		  1.1*c_1 + 1.1*c_2 + (1.1*1.1)*c_3 + c_4 == 1, ...
		  0*c_1 + 1*c_2 + (0*1)*c_3 + c_4 == 0);

disp(strcat('c1: ', num2str(double(sol_c_1))));
disp(strcat('c2: ', num2str(double(sol_c_2))));
disp(strcat('c3: ', num2str(double(sol_c_3))));
disp(strcat('c4: ', num2str(double(sol_c_4))));

[sol_c_5, sol_c_6, sol_c_7, sol_c_8] = ...
	solve(0*c_5 + 0*c_6 + (0*0)*c_7 + c_8 == 0, ...
 		  1*c_5 + 0*c_6 + (0*1)*c_7 + c_8 == 0, ...
 		  1.1*c_5 + 1.1*c_6 + (1.1*1.1)*c_7 + c_8 == 1, ...
 		  0*c_5 + 1*c_6 + (0*1)*c_7 + c_8 == 1);

disp(strcat('c5: ', num2str(double(sol_c_5))));
disp(strcat('c6: ', num2str(double(sol_c_6))));
disp(strcat('c7: ', num2str(double(sol_c_7))));
disp(strcat('c8: ', num2str(double(sol_c_8))));	

