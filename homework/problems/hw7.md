Homework #7 Problems
====================

Ben Draut / CS 450 / Mar. 19, 2014

##

**Compute the Fourier transform of $f(x,y) = \sin(x) \sin(y)$.**

We can observe that $f(x,y)$ is linearly separable into two functions, $\sin(x)$ and $\sin(y)$.

By the convolution theorem, we know that $\mathfrak{F}(f) = \mathfrak{F}(\sin(x)) * \mathfrak{F}(\sin(y))$.

$\mathfrak{F}(\sin(x)) * \mathfrak{F}(\sin(y)) = \frac{1}{2}i[\delta(u_1+s_1) - \delta(u_1-s_1)] * \frac{1}{2}i[\delta(u_2+s_2) - \delta(u_2-s_2)]$

$\mathfrak{F}(f(x,y)) = \frac{1}{2}i[\delta(u_1+s_1) - \delta(u_1-s_1) + \delta(u_2+s_2) - \delta(u_2-s_2)]$

##

**What is the Fourier Transform of $f(t) = \cos(16 \pi t) \cos(64 \pi t)$?**

$\mathfrak{F}(f) = \mathfrak{F}(\cos (16 \pi t)) * \mathfrak{F}(\cos (64 \pi t))$

$\mathfrak{F}(f) = \frac{1}{2}[\delta(u+16\pi) + \delta(u-16\pi)] * \frac{1}{2}[\delta(u+64\pi) + \delta(u-64\pi)]$

$\mathfrak{F}(f) = \frac{1}{2}[\delta(u-80\pi) + \delta(u-48\pi) + \delta(u+48\pi) + \delta(u+80\pi)]$