<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
</style>

Homework #4 Problems
====================

Ben Draut / CS 450 / Feb. 7, 2014

**1. Suppose the following signal was put into a system with the following impulse response. What would the result be?**

|          |   |   |   |   |   |   |   |   |   |
| -------- | - | - | - | - | - | - | - | - | - |
| Signal:  | 1 | 1 | 1 | 3 | 3 | 3 | 1 | 1 | 1 |
| Impulse: | 1 | 2 | 3 |   |   |   |   |   |   |

**Unlike with the previous assignment, treat the signal as an infinite one with samples before/after these as zeroes. Your result should thus have 11 elements. Remember to flip the kernel appropriately depending on which method you use to calculate the convolution.**

|                  |   |   |   |   |   |   |    |    |    |    |   |   |
| ---------------- | - | - | - | - | - | - | -- | -- | -- | -- | - | - |
| Padded Signal:   | 0 | 0 | 1 | 1 | 1 | 3 | 3  | 3  | 1  | 1  | 1 | 0 |
| Flipped Impulse: | 3 | 2 | 1 | ->| ->| ->| -> | -> | -> | -> | ->| ->|
| Result:		   | 0 | 1 | 3 | 6 | 8 | 12 | 18 | 16 | 12 | 6 | 5 | 3 |
| Cropped Result:  |   | 1 | 3 | 6 | 8 | 12 | 18 | 16 | 12 | 6 | 5 | 3 |

