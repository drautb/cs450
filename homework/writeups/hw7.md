<style type="text/css">
.left {
	float: left;
	max-width: 49%;
}
.right {
	float: right;
	max-width: 49%;
}
.col {
	float: left;
	width: 32%;
}
.clear {
	clear: both;
}
</style>

Homework #7 Writeup
===================

Ben Draut / CS 450 / Mar. 20, 2014

## A. 1-D Filtering

Original Data:

<div>
	<div class="left">
		<img src="../img/hw7/figure_1.png" alt="">
	</div>
	<div class="right">
	</div>
	<div class="clear"></div>
</div>

<div>
	<div class="left">
		Filter:
		<img src="../img/hw7/figure_2.png" alt="">
	</div>
	<div class="right">
		Data after filtering:
		<img src="../img/hw7/figure_3.png" alt="">
	</div>
	<div class="clear"></div>
</div>

For this data, I used a Gaussian filter of the form $H(u) = e^{-D^2(u)/2D_0^2}$ with cutoff frequency $D_0 = 20$.

This filter, as expected, had the effect of only passing the lower frequencies in the original data. We can see in the filtered data that frequencies were decreased as the approached 20, and increasingly supppressed until they didn't even appear after $u = 20$. One side effect is that the lower freqeuncies that we did want to pass were slightly decreased.


## B. 2-D Filtering / Convolution Theorem

**Use the Convolution Theorem to implement a 9x9 uniform spatial averaging filter in the Frequency Domain. That is, your code should produce the same results as you obtained in Homework #4 for spatial filtering with a uniform-averaging 9x9 kernel, but your implementation should use frequency-domain filtering, not convolution. Test your implementation using the image 2D_White_Box.pgm**

We start with a spatial averaging filter, $h(x,y)$, which is a 9x9 kernel of ones. From the convolution theorem, we know that we must take the Fourier transform of $h(x,y)$ in order to obtain a frequency domain representation of the filter, $H(u,v)$.

```matlab
imageData = imread('/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/2D_White_Box.png');
kernel = ones(9, 9);
paddedDims = [104 104];
H_u = fft2(double(kernel), paddedDims(1), paddedDims(2));
F = fft2(double(imageData), paddedDims(1), paddedDims(2));
FDF = H_u .* F;
fdf = ifft2(FDF);
fdf = fdf(8:size(imageData, 1), 8:size(imageData, 2));

figure;
figureHandle = gcf;
imshow(fdf, []);
print(figureHandle, 'figure_6.png', '-dpng');
close(figureHandle);
```

<div>
	<div class="col">
		2D_White_Box.pgm<br/>
		<img src="../img/hw7/figure_4.png" alt="">
	</div>
	<div class="col">
		Original Filtered Image:<br/>
		<img src="../img/hw7/figure_5.png" alt="">
	</div>
	<div class="col">
		Frequency Filtered Image:<br/>
		<img src="../img/hw7/figure_6.png" alt="">
	</div>
	<div class="clear"></div>
</div>

The results appear identical, as expected.


## C. Interference Pattern

**The image interfere.pgm an interference pattern of unknown spatial frequency, orientation, and magnitude. (It is, however, a single frequency.) Write a program to automatically find and eliminate it. Remember that you'll have to eliminate both that frequency and its inverse frequency.**

<div>
	<div class="left">
		Original Image
		<img src="../img/hw7/figure_7.png" alt="">
	</div>
	<div class="right">
		Transform
		<img src="../img/hw7/figure_8.png" alt="">
	</div>
	<div class="clear"></div>
</div>

Done:

<div>
	<div class="left">
		Filtered Transform
		<img src="../img/hw7/figure_9.png" alt="">
	</div>
	<div class="right">
		New Image
		<img src="../img/hw7/figure_10.png" alt="">
	</div>
	<div class="clear"></div>
</div>

I struggled with a lot of really simple parts to this section, mostly because I had some incorrect assumptions about what MATLAB was doing. Once I got that figured out, it worked great. 

Matlab Code:

```matlab
imageData = imread('interference.png');

figure;
figureHandle = gcf;
imshow(imageData);
print(figureHandle, 'figure_7.png', '-dpng');
close(figureHandle);

trans = fft2(imageData);
transShow = fftshift(trans);
transShow = abs(transShow);
transShow = log(transShow + 1);
transShow = mat2gray(transShow);

figure;
figureHandle = gcf;
imshow(transShow, []);
print(figureHandle, 'figure_8.png', '-dpng');
close(figureHandle);

% Modify the transform
modifiedTrans = trans;
mtReal = real(modifiedTrans);
mtImag = imag(modifiedTrans);
mag = sqrt(mtReal .^2 + mtImag .^2);
phase = atan2(mtImag, mtReal);

biggestDiff = 0;
biggestDiffX = 0;
biggestDiffY = 0;
biggestDiffAvg = 0;

% Find the most out of place freq
for x = 5:length(mag)
	for y = 5:length(mag)
		p = zeros(1,8);
		center = mag(x,y);
		total = 0;

		if x > 1 && y > 1
			p(1) = mag(x-1, y-1);
			total = total + 1;
		end

		if y > 1
			p(2) = mag(x, y-1);
			total = total + 1;
		end

		if x < length(mag) && y > 1
			p(3) = mag(x+1, y-1);
			total = total + 1;
		end

		if x > 1
			p(4) = mag(x-1, y);
			total = total + 1;
		end

		if x < length(mag)
			p(5) = mag(x+1, y);
			total = total + 1;
		end

		if x > 1 && y < length(mag)
			p(6) = mag(x-1, y+1);
			total = total + 1;
		end

		if y < length(mag)
			p(7) = mag(x, y+1);
			total = total + 1;
		end

		if x < length(mag) && y < length(mag)
			p(8) = mag(x+1, y+1);
			total = total + 1;
		end

		avg = sum(p) / total;

		thisDiff = center - avg;
		if thisDiff > biggestDiff
			disp(x);
			disp(y);
			disp(thisDiff);
			biggestDiff = thisDiff;
			biggestDiffAvg = avg;
			biggestDiffX = x;
			biggestDiffY = y;
		end
	end
end

mag(biggestDiffX, biggestDiffY) = biggestDiffAvg;
mag(length(mag) - biggestDiffX + 2, length(mag) - biggestDiffY + 2) = biggestDiffAvg;

% Recompute the new values
newReal = mag .* cos(phase);
newImag = mag .* sin(phase);
modifiedTrans = newReal + 1i*newImag;

transShow = ifftshift(modifiedTrans);
transShow = abs(transShow);
transShow = log(transShow + 1);
transShow = mat2gray(transShow);

figure;
figureHandle = gcf;
imshow(transShow, []);
print(figureHandle, 'figure_9.png', '-dpng');
close(figureHandle);

newImage = ifft2(modifiedTrans);

figure;
figureHandle = gcf;
imshow(uint8(real(newImage)), []);
print(figureHandle, 'figure_10.png', '-dpng');
close(figureHandle);

```