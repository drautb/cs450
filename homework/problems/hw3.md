<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
</style>

Homework #3 Problems
====================

Ben Draut / CS 450 / Jan. 29, 2014

**1. Compute by hand the spatial filtering of the following signal by the following mask/kernel:**

|         |   |   |   |   |   |   |   |   |   |
| ------- | - | - | - | - | - | - | - | - | - |
| Signal: | 1 | 1 | 2 | 3 | 3 | 3 | 2 | 1 | 1 |
| Kernel: | 1 | 1 | 1 |   |   |   |   |   |   |

**You only need to produce an output signal that is as long as the input signal. How do you handle the boundaries of the finite-length signal?**

We'll handle the boundaries by padding the signal with zeros as needed. After applying the mask, we get:

|         |   |   |   |   |   |   |   |   |   |
| ------- | - | - | - | - | - | - | - | - | - | 
| Result: | 2 | 4 | 6 | 8 | 9 | 8 | 6 | 4 | 2 | 


**2. Compute by hand the spatial filtering of the following signal by the following mask:**

|         |   |   |   |   |   |   |   |   |   |
| ------- | - | - | - | - | - | - | - | - | - |
| Signal: | 1 | 1 | 2 | 3 | 3 | 3 | 2 | 1 | 1 |
| Kernel: | 1 | 2 | 1 |   |   |   |   |   |   |
 
**As before, you only need to produce an output signal that is as long as the input signal.**

|         |   |   |   |    |    |    |   |   |   |
| ------- | - | - | - | -- | -- | -- | - | - | - |
| Result: | 3 | 5 | 8 | 11 | 12 | 11 | 8 | 5 | 3 |


**3. Compute by hand the spatial filtering of the following image and mask:**

**Image:**

|   |   |   |   |    |
| - | - | - | - | -- |
| 0 | 1 | 2 | 3 | 5  |
| 1 | 2 | 3 | 5 | 7  |
| 2 | 3 | 5 | 7 | 8  |
| 3 | 5 | 7 | 8 | 9  |
| 5 | 7 | 8 | 9 | 10 |

**Kernel:**

|   |   |   |
| - | - | - |
| 1 | 1 | 1 |
| 1 | 2 | 1 |
| 1 | 1 | 1 |

**Your result should be 5 x 5. State how you handle cases on the boundaries of the image.**

Result:

|    |    |    |    |    |
| -- | -- | -- | -- | -- |
| 4  | 10 | 18 | 28 | 25 |
| 10 | 21 | 34 | 50 | 42 |
| 18 | 34 | 50 | 66 | 52 |
| 28 | 50 | 66 | 79 | 60 |
| 25 | 42 | 52 | 60 | 46 |
 
Again, I padded the image with zeros around the edge to handle the boundaries.

**Textbook: 3.19b: Propose a technique for updating the median as the center of neighborhood is moved from pixel to pixel. The idea here is to take advantage of the reusable information as you slide the median-filtering window across the image. Can you avoid having to sort/select again from scratch each time? (Transition note: this is 3.20b in the 2nd edition of the text.)**

For an $n$ by $n$ median filter, there are $n^2$ neighborhood pixels to consider for each pixel in the image. However, between adjacent pixels, at most $n$ intensities will change. Thus, rather than rebuilding the entire list of intensities for each pixel, we can remove the $n$ intensities that will no longer be in the neighborhood, and insert in sorted order the new $n$ intensities, and then retrieve the median. This saves an increasing amount of computation as $n$ grows.

