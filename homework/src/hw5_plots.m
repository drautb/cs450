s = 8; N = 128;
t = (0:N-1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% SINE WAVE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
figureHandle = gcf;
sine = sin(2*pi*s*t/N);
plot(t, sine);
grid on;
title('f(t) = sin(2*pi*s*t/N)');
xlabel('t');
ylabel('f(t)');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_1.png', '-dpng');
close(figureHandle);
sineTrans = fft(sine);

figure;
figureHandle = gcf;
realPart = real(sineTrans)/(N/2);
realPart(1) = real(sineTrans(1))/N;
realPart(N/2+1) = real(sineTrans(N/2+1))/N;
plot(realPart);
grid on;
title('Sine - Real Part');
xlabel('Frequency');
ylabel('Scaled Amplitude');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_2.png', '-dpng');
close(figureHandle);

figure;
figureHandle = gcf;
plot(imag(sineTrans)/(N/2));
grid on;
title('Sine - Imaginary Part');
xlabel('Frequency');
ylabel('Scaled Amplitude');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_3.png', '-dpng');
close(figureHandle);

% I normalized by N/2 to get the magnitude to match the original signal.
figure;
figureHandle = gcf;
reals = real(sineTrans) / (N / 2);
imags = imag(sineTrans) / (N / 2);
sineMag = sqrt(reals.^2 + imags.^2);
plot(sineMag);
grid on;
title('Sine - Magnitude');
xlabel('Frequency');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_4.png', '-dpng');
close(figureHandle);

figure;
figureHandle = gcf;
sinePhase = atan2(imags, reals);
plot(sinePhase);
grid on;
title('Sine - Phase');
xlabel('Frequency');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_5.png', '-dpng');
close(figureHandle);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% COSINE WAVE %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
figureHandle = gcf;
cosine = cos(2*pi*s*t/N);
plot(t, cosine);
grid on;
title('f(t) = cos(2*pi*s*t/N)');
xlabel('t');
ylabel('f(t)');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_6.png', '-dpng');
close(figureHandle);
cosineTrans = fft(cosine);

figure;
figureHandle = gcf;
realPart = real(cosineTrans)/(N/2);
realPart(1) = real(cosineTrans(1))/N;
realPart(N/2+1) = real(cosineTrans(N/2+1))/N;
plot(realPart);
grid on;
title('Cosine - Real Part');
xlabel('Frequency');
ylabel('Scaled Amplitude');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_7.png', '-dpng');
close(figureHandle);

figure;
figureHandle = gcf;
plot(imag(cosineTrans)/(N/2));
grid on;
title('Cosine - Imaginary Part');
xlabel('Frequency');
ylabel('Scaled Amplitude');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_8.png', '-dpng');
close(figureHandle);

figure;
figureHandle = gcf;
reals = real(cosineTrans) / (N / 2);
imags = imag(cosineTrans) / (N / 2);
cosineMag = sqrt(reals.^2 + imags.^2);
plot(cosineMag);
grid on;
title('Cosine - Magnitude');
xlabel('Frequency');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_9.png', '-dpng');
close(figureHandle);

figure;
figureHandle = gcf;
cosinePhase = atan2(imags, reals);
plot(cosinePhase);
grid on;
title('Cosine - Phase');
xlabel('Frequency');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_10.png', '-dpng');
close(figureHandle);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% SINE/COSINE SUM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
figureHandle = gcf;
scSum = sin(2*pi*s*t/N) + cos(2*pi*s*t/N);
plot(t, scSum);
grid on;
title('f(t) = sin(2*pi*s*t/N) + cos(2*pi*s*t/N)');
xlabel('t');
ylabel('f(t)');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_11.png', '-dpng');
close(figureHandle);
scSumTrans = fft(scSum);

figure;
figureHandle = gcf;
realPart = real(scSumTrans)/(N/2);
realPart(1) = real(scSumTrans(1))/N;
realPart(N/2+1) = real(scSumTrans(N/2+1))/N;
plot(realPart);
grid on;
title('Sine/Cosine Sum - Real Part');
xlabel('Frequency');
ylabel('Scaled Amplitude');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_12.png', '-dpng');
close(figureHandle);

figure;
figureHandle = gcf;
plot(imag(scSumTrans)/(N/2));
grid on;
title('Sine/Cosine Sum - Imaginary Part');
xlabel('Frequency');
ylabel('Scaled Amplitude');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_13.png', '-dpng');
close(figureHandle);

figure;
figureHandle = gcf;
reals = real(scSumTrans) / (N / 2);
imags = imag(scSumTrans) / (N / 2);
scSumMag = sqrt(reals.^2 + imags.^2);
plot(scSumMag);
grid on;
title('Sine/Cosine Sum - Magnitude');
xlabel('Frequency');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_14.png', '-dpng');
close(figureHandle);

figure;
figureHandle = gcf;
scSumPhase = atan2(imags, reals);
plot(scSumPhase);
grid on;
title('Sine/Cosine Sum - Phase');
xlabel('Frequency');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_15.png', '-dpng');
close(figureHandle);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% RECT FUNCTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
figureHandle = gcf;
load 'data/1D_Rect128.dat';
rectData = X1D_Rect128;
N = length(rectData);
plot(rectData);
grid on;
title('Rect Function');
xlabel('t');
ylabel('f(t)');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_16.png', '-dpng');
close(figureHandle);

figure;
figureHandle = gcf;
rectTrans = fft(rectData);
reals = real(rectTrans) / (N / 2);
imags = imag(rectTrans) / (N / 2);
rectMag = sqrt(reals.^2 + imags.^2);
plot(rectMag);
grid on;
title('Rect Function - Magnitude');
xlabel('Frequency');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_17.png', '-dpng');
close(figureHandle);

figure;
figureHandle = gcf;
rectPow = reals.^2 + imags.^2;
plot(rectPow);
grid on;
title('Rect Function - Power Spectrum');
xlabel('Frequency');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_18.png', '-dpng');
close(figureHandle);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% GAUSSIAN FUNCTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
figureHandle = gcf;
load 'data/1D_Gauss128.dat';
gaussData = X1D_Gauss128;
N = length(gaussData);
plot(gaussData);
grid on;
title('Gaussian Function');
xlabel('t');
ylabel('f(t)');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_19.png', '-dpng');
close(figureHandle);

figure;
figureHandle = gcf;
gaussTrans = fft(gaussData);
reals = real(gaussTrans) / (N / 2);
imags = imag(gaussTrans) / (N / 2);
gaussMag = sqrt(reals.^2 + imags.^2);
plot(gaussMag);
grid on;
title('Gaussian Function - Magnitude');
xlabel('Frequency');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_20.png', '-dpng');
close(figureHandle);

figure;
figureHandle = gcf;
gaussPow = reals.^2 + imags.^2;
plot(gaussPow);
grid on;
title('Gaussian Function - Power Spectrum');
xlabel('Frequency');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_21.png', '-dpng');
close(figureHandle);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FREQUENCY ANALYSIS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
figureHandle = gcf;
load 'data/1D_Signal.dat';
signalData = X1D_Signal;
plot(signalData);
grid on;
title('1D_Signal.dat');
xlabel('t');
ylabel('f(t)');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_22.png', '-dpng');
close(figureHandle);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DISCOVERING THE TRANSFER FUNCTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear;
load 'data/1D_Rect128.dat';
load 'data/1D_Output128.dat';
f_t = X1D_Rect128;
g_t = X1D_Output128;

figure;
figureHandle = gcf;
plot(f_t);
title('f(t)');
xlabel('t');
ylabel('f(t)');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_23.png', '-dpng');
close(figureHandle);

figure;
figureHandle = gcf;
plot(g_t);
title('g(t)');
xlabel('t');
ylabel('g(t)');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_24.png', '-dpng');
close(figureHandle);

N = length(f_t);
f_tTrans = fft(f_t);
g_tTrans = fft(g_t);
H = g_tTrans/f_tTrans;
reals = real(H) / (N / 2);
imags = imag(H) / (N / 2);
HMag = sqrt(reals.^2 + imags.^2);
figure
figureHandle = gcf;
plot(HMag);
grid on;
title('H(u) Magnitude');
xlabel('u');
ylabel('|H(u)|');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_25.png', '-dpng');
close(figureHandle);

figure;
figureHandle = gcf;
h_t = ifft(H);
plot(h_t);
grid on;
title('h(t)');
xlabel('t');
ylabel('h(t)');
print(figureHandle, '/home/drautb/Dropbox/Winter 2014/CS 450/cs450/homework/img/hw5/figure_26.png', '-dpng');
close(figureHandle);
