<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
</style>

Homework #3 Writeup
===================

Ben Draut / CS 450 / Jan. 30, 2014

## Arithmetic Operations

I decided to do option #2 for the arithmetic operations, that is, averaging multiple frames to improve the quality of a noisy image. Here are the results that I obtained:

<table>
    <tr>
        <th>2 Images</th>
        <th>5 Images</th>
        <th>10 Images</th>
        <th>20 Images</th>
        <th>40 Images</th>
    </tr>
    <tr>
        <td><img src="../img/Frames/avg2.png"/></td>
        <td><img src="../img/Frames/avg5.png"/></td>
        <td><img src="../img/Frames/avg10.png"/></td>
        <td><img src="../img/Frames/avg20.png"/></td>
        <td><img src="../img/Frames/avg40.png"/></td>
    </tr>
</table>

Averaging frames works because the noise is random. Depending on the frame, different pixels are more heavily corrupted by the noise. Statistically, the more frames we get, the more likely an average is to yield a truer representation. I would say that between 10 and 20 frames is enough based on the above example. Although minor improvements can be seen when 40 frames were used, the overall effect was negligible. The image didn't achieve any significant discernible increase in quality.

## Mean Filter

This was interesting. For a small filter size (< 5x5), the mean filter did actually improve the quality a little bit, but at the cost of some blur. For the 7x7 and 9x9 filters however, the blur was so bad that you could no longer discern any detail.

<table>
    <tr>
        <th>3 x 3</th>
        <th>5 x 5</th>
        <th>7 x 7</th>
        <th>9 x 9</th>
    </tr>
    <tr>
        <td><img src="../img/NoisyGull/NoisyGull-mean-3x3.png"/></td>
        <td><img src="../img/NoisyGull/NoisyGull-mean-5x5.png"/></td>
        <td><img src="../img/NoisyGull/NoisyGull-mean-7x7.png"/></td>
        <td><img src="../img/NoisyGull/NoisyGull-mean-9x9.png"/></td>
    </tr>
</table>

## Median Filter

The median filter was more resistant to blurring than the mean filter. It improved the quality of the image as well as the mean filter did, but without the cost of blurriness for the smaller filters.

<table>
    <tr>
        <th>3 x 3</th>
        <th>5 x 5</th>
        <th>7 x 7</th>
    </tr>
    <tr>
        <td><img src="../img/NoisyGull/NoisyGull-median-3x3.png"/></td>
        <td><img src="../img/NoisyGull/NoisyGull-median-5x5.png"/></td>
        <td><img src="../img/NoisyGull/NoisyGull-median-7x7.png"/></td>
    </tr>
</table>

