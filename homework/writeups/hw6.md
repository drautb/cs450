<style type="text/css">
.left {
	float: left;
	max-width: 49%;
}
.right {
	float: right;
	max-width: 49%;
}
.clear {
	clear: both;
}
</style>

Homework #6 Writeup - FT Part #2
================================

Ben Draut / CS 450 / Mar. 1, 2014

## Simple Sines and Cosines

**Make a two-dimensional image that is a sinusoid in one direction and constant in the other:**
 
$f(x,y) = \sin(2 \pi s x / N)$

**Use $N = 256$ and various values of $s$. Display each image and the magnitude of their respective Fourier Transforms.**

<div>
	<div class="left">
		<img src="../img/hw6/figure_1.png" alt="">
	</div>
	<div class="right">
		<img src="../img/hw6/figure_2.png" alt="">
	</div>
	<div class="clear"></div>
</div>


<div>
	<div class="left">
		<img src="../img/hw6/figure_3.png" alt="">
	</div>
	<div class="right">
		<img src="../img/hw6/figure_4.png" alt="">
	</div>
	<div class="clear"></div>
</div>


<div>
	<div class="left">
		<img src="../img/hw6/figure_5.png" alt="">
	</div>
	<div class="right">
		<img src="../img/hw6/figure_6.png" alt="">
	</div>
	<div class="clear"></div>
</div>


**Create another two-dimensional image, again with a sinusoid in one direction and constant in the other, only swap the two directions: $f(x,y) = \sin(2 \pi s y / N)$. Again use $N = 256$ but choose a different value for the frequency than that used in Part A. Again display the image and the magnitude of its Fourier Transform.**

<div>
	<div class="left">
		<img src="../img/hw6/figure_7.png" alt="">
	</div>
	<div class="right">
		<img src="../img/hw6/figure_8.png" alt="">
	</div>
	<div class="clear"></div>
</div>


<div>
	<div class="left">
		<img src="../img/hw6/figure_9.png" alt="">
	</div>
	<div class="right">
		<img src="../img/hw6/figure_10.png" alt="">
	</div>
	<div class="clear"></div>
</div>


<div>
	<div class="left">
		<img src="../img/hw6/figure_11.png" alt="">
	</div>
	<div class="right">
		<img src="../img/hw6/figure_12.png" alt="">
	</div>
	<div class="clear"></div>
</div>

## Addition

**Add the two images from Part A together. Display the magnitude part of the Fourier Transform of the sum and explain.**

I added together $\sin(2\pi s x / N)$ where $s = 5$ and $\sin(2\pi s y / N)$ where $s = 10$ to get the following image:

<div>
	<div class="left">
		<img src="../img/hw6/figure_13.png" alt="">
	</div>
	<div class="right">
		<img src="../img/hw6/figure_14.png" alt="">
	</div>
	<div class="clear"></div>
</div>

This makes sense based on our previous observations. This image is composed of a sine wave with frequency 5 in the x direction, and 10 in the y direction. The fourier transform shows this by displaying brighter points at $\pm 5$ in x, and $\pm 10$ in y.

## Rotation

**Using the image created in Part B that is the sum of a sinusoid in one direction and a sinusoid in the other, rotate the image by an arbitrary amount (use Photoshop, GIMP, or comparable tools) and display the result. Display the magnitude part of the Fourier Transform of the rotated image and explain.**

I rotated the previous image by 30 degrees using MATLAB's `imrotate`.

<div>
	<div class="left">
		<img src="../img/hw6/figure_15.png" alt="">
	</div>
	<div class="right">
		<img src="../img/hw6/figure_16.png" alt="">
	</div>
	<div class="clear"></div>
</div>

This one still baffle's me a bit. I think the most prevalent lines that go near the center represent the edges of the rotated image. (Where the signal meets the black.)

## Multiplication

**Do the same thing as in Part B, except multiply the two images instead of adding them. Explain why you get this result.**

<div>
	<div class="left">
		<img src="../img/hw6/figure_17.png" alt="">
	</div>
	<div class="right">
		<img src="../img/hw6/figure_18.png" alt="">
	</div>
	<div class="clear"></div>
</div>

This was interesting. Multiplying the two signals together caused the image to look more like a checkerboard. This had the result of giving us a grid of four points in the transform, two for each frequency, offset from eachother by 1/2.

## Magnitude and Phase

**Ball:**

<div>
	<div class="left">
		<img src="../img/hw6/ball.png" alt="">
	</div>
	<div class="right">
		<img src="../img/hw6/figure_19.png" alt="">
	</div>
	<div class="clear"></div>
</div>

**Gull:**

<div>
	<div class="left">
		<img src="../img/hw6/gull.png" alt="">
	</div>
	<div class="right">
		<img src="../img/hw6/figure_20.png" alt="">
	</div>
	<div class="clear"></div>
</div>

**Combined:**

<div>
	<div class="left">
		<h6>Ball Magnitude + Gull Phase</h6>
		<img src="../img/hw6/ball-mag.png" alt="">
	</div>
	<div class="right">
		<h6>Gull Magnitude + Ball Phase</h6>
		<img src="../img/hw6/gull-mag.png" alt="">
	</div>
	<div class="clear"></div>
</div>

I was not expecting these results at all. I thought that the magnitude would have a greater influence than the phase, but it appears the opposite is true. The Image that used the Gull's phase looks more like the gull, and the same goes for the ball. Very cool.
