<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
img {
    max-width: 150px;
    max-height: 150px;
}
</style>

Homework #4 Writeup
===================

Ben Draut / CS 450 / Feb. 7, 2014

## Uniform Averaging Kernels

<table>
    <tr>
        <th>3 x 3</th>
        <th>5 x 5</th>
        <th>7 x 7</th>
        <th>9 x 9</th>
    </tr>
    <tr>
        <td><img src="../img/hw4/avg3x3.png"/></td>
        <td><img src="../img/hw4/avg5x5.png"/></td>
        <td><img src="../img/hw4/avg7x7.png"/></td>
        <td><img src="../img/hw4/avg9x9.png"/></td>
    </tr>
</table>

The uniform averaging kernels simply blurred the boundary between the white and black boxes. This is to be expected considering what we discovered in past assignments. Averaging kernels have a blurring effect on their subjects.

## Sobel/Laplacian Kernels

<table>
    <tr>
        <th>Sobel - X</th>
        <th>Sobel - Y</th>
        <th>Sobel - Both</th>
        <th>Laplacian</th>
    </tr>
    <tr>
        <td><img src="../img/hw4/sobel-x.png"/></td>
        <td><img src="../img/hw4/sobel-y.png"/></td>
        <td><img src="../img/hw4/sobel-both.png"/></td>
        <td><img src="../img/hw4/laplacian.png"/></td>
    </tr>
</table>

The Sobel kernels discovered the boundaries in the image, which again was to be expected. The Sobel kernel is based on the derivative of the image. Along edges in the image, the derivative is highest, thus causing them to be highlighted by the kernel. The x-kernel discovers vertical boundaries, while the y-kernel discovers horizontal edges. The Laplacian kernel performed similarly, however it discovers both horizontal and vertical edges since it uses the second derivative of the image.

## Sobel Gradients

<table>
    <tr>
        <th>2D White Box</th>
        <th>Blocks</th>
    </tr>
    <tr>
        <td><img src="../img/hw4/box-gradient.png"/></td>
        <td><img src="../img/hw4/blocks-gradient.png"/></td>
    </tr>
</table>

These gradients show where the edges of in the images are most prevalent. The original kernel just outputs values based on the derivatives, which aren't always smooth. By computing a gradient image, the values are based on the magnitude of the changes, so the image is smoother.

## Blurred Gradients

<table style="max-width: 100%">
    <tr>
        <th>3x3</th>
        <th>5x5</th>
        <th>7x7</th>
        <th>9x9</th>
    </tr>
    <tr>
        <td><img src="../img/hw4/blurred-blocks-3x3.png"/></td>
        <td><img src="../img/hw4/blurred-blocks-5x5.png"/></td>
        <td><img src="../img/hw4/blurred-blocks-7x7.png"/></td>
        <td><img src="../img/hw4/blurred-blocks-9x9.png"/></td>
    </tr>
</table>

As we blur the images, the gradients become less and less pronounced. This makes sense because the edges are no longer as crisp. The lines in the gradient image fade as the edges become more blurry.

## Sharpened Blocks

![](../img/hw4/sharp-blocks.png)

## Addtional Images

<table>
    <tr>
        <th>Sobel X and Y</th>
        <th>Laplacian</th>
    </tr>
    <tr>
        <td><img src="../img/hw4/blocks-sobel-both.png"/></td>
        <td><img src="../img/hw4/blocks-laplacian.png"/></td>
    </tr>
</table>
